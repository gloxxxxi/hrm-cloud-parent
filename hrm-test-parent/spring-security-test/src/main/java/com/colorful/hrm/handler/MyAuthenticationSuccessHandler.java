package com.colorful.hrm.handler;

import com.alibaba.fastjson.JSON;
import com.colorful.hrm.util.AjaxResult;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component //认证成功处理
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        AjaxResult me = AjaxResult.me();
        writer.write(JSON.toJSONString(me));
        writer.flush();
        writer.close();
    }
}
