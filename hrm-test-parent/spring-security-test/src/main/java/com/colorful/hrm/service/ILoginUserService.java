package com.colorful.hrm.service;

import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.domain.LoginUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yaosang
 * @since 2022-01-02
 */
public interface ILoginUserService extends IService<LoginUser> {

}
