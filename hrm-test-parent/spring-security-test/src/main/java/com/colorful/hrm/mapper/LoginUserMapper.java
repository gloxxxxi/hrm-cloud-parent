package com.colorful.hrm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.colorful.hrm.domain.LoginUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yaosang
 * @since 2022-01-02
 */
public interface LoginUserMapper extends BaseMapper<LoginUser> {

}
