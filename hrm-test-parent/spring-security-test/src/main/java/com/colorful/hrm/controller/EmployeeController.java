package com.colorful.hrm.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    @RequestMapping("/employee/list")
    //权限控制：方案2：注解实现，可以支持restfull
    @PreAuthorize("hasAuthority('employee:list')")
    public String list(){
        return "employee.list";
    }
    @PreAuthorize("hasAuthority('employee:add')")
    @RequestMapping("/employee/add")
    public String add(){
        return "employee.add";
    }
    @PreAuthorize("hasAuthority('employee:update')")
    @RequestMapping("/employee/update")
    public String update(){
        return "employee.update";
    }
    @PreAuthorize("hasAuthority('employee:del')")
    @RequestMapping("/employee/del")
    public String delete(){
        return "employee.del";
    }
}