package com.colorful.hrm.domain;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yaosang
 * @since 2022-01-02
 */
@TableName("t_login_user")
public class LoginUser extends Model<LoginUser> {//implements UserDetails {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String username;
    private String password;
    private Integer type;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LoginUser{" +
        ", id=" + id +
        ", username=" + username +
        ", password=" + password +
        ", type=" + type +
        "}";
    }

    /*@Override //账号是否过期
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override //账号是否锁定
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override //密码是否过期
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override //是否可用
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override //返回权限
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public LoginUser(String username, String password) {
        this.username = username;
        this.password = password;
    }
    public LoginUser() {
    }*/

}
