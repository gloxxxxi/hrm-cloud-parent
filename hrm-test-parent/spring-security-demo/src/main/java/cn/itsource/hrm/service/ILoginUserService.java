package com.colorful.hrm.service;

import com.colorful.hrm.domain.LoginUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yaosang
 * @since 2022-01-02
 */
public interface ILoginUserService extends IService<LoginUser> {

}
