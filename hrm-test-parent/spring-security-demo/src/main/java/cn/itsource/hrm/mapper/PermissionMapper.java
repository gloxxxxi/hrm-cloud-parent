package com.colorful.hrm.mapper;

import com.colorful.hrm.domain.Permission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yaosang
 * @since 2022-01-02
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    List<Permission> loadPermissionsByUserId(Long uid);
}
