package cn.itsoruce.hrm.handler;

import cn.itsoruce.hrm.config.RabbitMqConfig;
import cn.itsoruce.hrm.domain.User;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MessageHandler {

//    @RabbitListener(queues = {RabbitMqConfig.QUEUE_INFORM_SMS},containerFactory = "rabbitListenerContainerFactory" )
    @RabbitListener(queues = {RabbitMqConfig.QUEUE_INFORM_SMS} )
    public void smsHandle(User user, Message message, Channel channel){
        System.out.println(user);
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            channel.basicAck(deliveryTag,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    @RabbitListener(queues = {RabbitMqConfig.QUEUE_INFORM_EMAIL},containerFactory = "rabbitListenerContainerFactory")
    @RabbitListener(queues = {RabbitMqConfig.QUEUE_INFORM_EMAIL})
    public void emailHandle(User user, Message message, Channel channel){
        System.out.println(user);
    }
    //监听短信队列
    //@RabbitListener(queues = {RabbitMqConfig.QUEUE_INFORM_SMS})
    public void smsHandle(String msg, Message message, Channel channel){
        System.out.println("短信处理！！！！！！！！！！！！");
        System.out.println(msg);
        MessageProperties mp = message.getMessageProperties();
        System.out.println(mp.getDeliveryTag());
    }
    //监听短信队列
    //@RabbitListener(queues = {RabbitMqConfig.QUEUE_INFORM_EMAIL})
    public void emailHandle(String msg, Message message, Channel channel){
        System.out.println("邮件处理！！！！！！！！！！！！");
        System.out.println(msg);
        MessageProperties mp = message.getMessageProperties();
        System.out.println(mp.getDeliveryTag());
    }
}
