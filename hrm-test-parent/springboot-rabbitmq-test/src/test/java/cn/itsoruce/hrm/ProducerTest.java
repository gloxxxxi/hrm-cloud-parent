package cn.itsoruce.hrm;

import cn.itsoruce.hrm.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//通过测试模拟生产者，以后代码中调用
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class ProducerTest {
    //交换机名字
    private static  final  String EXCHANGE_TOPICS_INFORM = "exchange_topics_inform";
    //直接通过模板发送
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Test
    public void sendTest() throws Exception{

        //1 发送字符串消息
        //String message = "sms message!";
        //rabbitTemplate.convertAndSend(EXCHANGE_TOPICS_INFORM,"inform.sms",message);
        //rabbitTemplate.convertAndSend(EXCHANGE_TOPICS_INFORM,"inform.email",message);

        //2 发送对象
        //交换机回调,无论是找到还是找不到都要回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                System.out.println("交换机回调");
                System.out.println("correlationData:"+correlationData);
                System.out.println("ack："+ack); //找到交换机会签收
                System.out.println("cause："+cause);
            }
        });

        //队列接收失败回调
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            @Override
            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
                System.out.println("队列接收失败回调");
                System.out.println("message:"+message);
                System.out.println("replyCode："+replyCode);
                System.out.println("replyText："+replyText);
                System.out.println("exchange："+exchange);
                System.out.println("routingKey："+routingKey);
            }
        });
        User user = new User(1L, "zs");
        rabbitTemplate.convertAndSend(EXCHANGE_TOPICS_INFORM,"inform.sms",user);
    }
}
