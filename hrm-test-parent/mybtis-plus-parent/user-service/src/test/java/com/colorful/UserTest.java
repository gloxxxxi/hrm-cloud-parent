package com.colorful;

import com.colorful.hrm.UserServiceApplication;
import com.colorful.hrm.domain.VipUser;
import com.colorful.hrm.service.IVipUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author SuperJellyfish
 * @description TODO
 * @date 2021/12/30 16:43
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserServiceApplication.class)
public class UserTest {
    @Autowired
    private IVipUserService vipUserService;
    @Test
    public void testAdd() throws Exception {
        VipUser vipUser = new VipUser();
        vipUser.setNickName("2333");
        vipUserService.insert(vipUser);
    }
    @Test
    public void testGetOne() throws Exception {
        VipUser vipUser = vipUserService.selectById(1L);
        System.out.println("vipUser = " + vipUser);
    }
    @Test
    public void testUpdate() throws Exception {
        VipUser vipUser = vipUserService.selectById(1L);
        vipUser.setNickName("super-jellyfish");
        vipUserService.updateById(vipUser);
    }
    @Test
    public void testDelete() throws Exception {
        vipUserService.deleteById(1L);
    }

    @Test
    public void testList() throws Exception {
        List<String> strings = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        String str = "ss";
        String s = str + "ss";
        System.out.println("s = " + s);

    }
}
