package com.colorful.hrm.service;

import com.colorful.hrm.domain.VipUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员登录账号 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2021-12-30
 */
public interface IVipUserService extends IService<VipUser> {

}
