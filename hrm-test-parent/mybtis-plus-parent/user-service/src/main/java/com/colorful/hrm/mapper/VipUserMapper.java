package com.colorful.hrm.mapper;

import com.colorful.hrm.domain.VipUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员登录账号 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2021-12-30
 */
public interface VipUserMapper extends BaseMapper<VipUser> {

}
