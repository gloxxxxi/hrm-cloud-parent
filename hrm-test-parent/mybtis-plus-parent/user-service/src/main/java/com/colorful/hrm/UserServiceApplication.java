package com.colorful.hrm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author SuperJellyfish
 * @description 测试服务启动类
 * @date 2021/12/30 16:44
 */
@SpringBootApplication
@MapperScan("com.colorful.hrm.mapper")
public class UserServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }
}
