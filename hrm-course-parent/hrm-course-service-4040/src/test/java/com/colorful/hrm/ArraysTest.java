package com.colorful.hrm;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author SuperJellyfish
 * @description TODO
 * @date 2022/1/8 9:16
 */

public class ArraysTest {
    @Test
    public void test() throws Exception {
        String[] strs = {"1", "2"};
        List<String> strings  = Arrays.asList(strs);
        strings.add("3");
        System.out.println("strings = " + strings);
    }
}
