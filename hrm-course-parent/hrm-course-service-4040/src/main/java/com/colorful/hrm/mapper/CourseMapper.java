package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.Course;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.colorful.hrm.query.CourseQuery;

import java.util.List;
import com.baomidou.mybatisplus.plugins.Page;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface CourseMapper extends BaseMapper<Course> {

    List<Course> loadPageList(Page page,CourseQuery query);

    void online(List<Long> ids);

    void offline(List<Long> ids);
}
