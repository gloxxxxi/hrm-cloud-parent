package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.CourseType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 课程目录 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface CourseTypeMapper extends BaseMapper<CourseType> {

    /**
     * 嵌套查询
     * @param pid
     * @return
     */
    List<CourseType> getTreeByNested(Long pid);


}
