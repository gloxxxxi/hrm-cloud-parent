package com.colorful.hrm.service;

import com.colorful.hrm.entity.CourseOnlineLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-08
 */
public interface ICourseOnlineLogService extends IService<CourseOnlineLog> {

}
