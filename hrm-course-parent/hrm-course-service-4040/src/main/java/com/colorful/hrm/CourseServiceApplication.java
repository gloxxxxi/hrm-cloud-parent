package com.colorful.hrm;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

/**
 * @author SuperJellyfish
 * @description 启动类 - 系统中心
 * EnableCaching //启用注解缓存
 * @date 2022/1/2 11:05
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.colorful.hrm.mapper")
@EnableFeignClients
@EnableCaching
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CourseServiceApplication {
    static Logger logger= LoggerFactory.getLogger(CourseServiceApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(CourseServiceApplication.class, args);
    }
}
