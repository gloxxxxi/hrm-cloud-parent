package com.colorful.hrm.service;

import com.colorful.hrm.entity.CourseDetail;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface ICourseDetailService extends IService<CourseDetail> {

}
