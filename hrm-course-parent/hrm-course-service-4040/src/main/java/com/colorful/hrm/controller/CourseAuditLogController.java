package com.colorful.hrm.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.colorful.hrm.service.ICourseAuditLogService;
import com.colorful.hrm.entity.CourseAuditLog;
import com.colorful.hrm.query.CourseAuditLogQuery;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/courseAuditLog")
public class CourseAuditLogController {
    @Autowired
    public ICourseAuditLogService courseAuditLogService;


    /**
     * 保存和修改公用的
     * @param courseAuditLog  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody CourseAuditLog courseAuditLog){
        try {
            if( courseAuditLog.getId()!=null) {
                courseAuditLogService.updateById(courseAuditLog);
            } else {
                courseAuditLogService.insert(courseAuditLog);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            courseAuditLogService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public CourseAuditLog get(@PathVariable("id")Long id)
    {
        return courseAuditLogService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public List<CourseAuditLog> list(){

        return courseAuditLogService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<CourseAuditLog> json(@RequestBody CourseAuditLogQuery query)
    {
        Page<CourseAuditLog> page = new Page<CourseAuditLog>(query.getPage(),query.getRows());
        page = courseAuditLogService.selectPage(page);
        return new PageList<CourseAuditLog>(page.getTotal(),page.getRecords());
    }
    @GetMapping("/course/{courseId}")
    public List<CourseAuditLog> getByCourseId(@PathVariable("courseId")Long courseId)
    {
        return courseAuditLogService.selectList(new EntityWrapper<CourseAuditLog>().eq("course_id",courseId));
    }
}
