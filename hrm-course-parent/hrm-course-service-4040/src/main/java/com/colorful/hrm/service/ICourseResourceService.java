package com.colorful.hrm.service;

import com.colorful.hrm.entity.CourseResource;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface ICourseResourceService extends IService<CourseResource> {

}
