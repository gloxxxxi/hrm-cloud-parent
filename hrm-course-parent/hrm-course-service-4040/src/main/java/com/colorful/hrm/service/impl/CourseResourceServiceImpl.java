package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.CourseResource;
import com.colorful.hrm.mapper.CourseResourceMapper;
import com.colorful.hrm.service.ICourseResourceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
@Service
public class CourseResourceServiceImpl extends ServiceImpl<CourseResourceMapper, CourseResource> implements ICourseResourceService {

}
