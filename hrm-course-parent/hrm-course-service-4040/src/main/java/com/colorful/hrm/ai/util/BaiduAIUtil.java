package com.colorful.hrm.ai.util;


import com.colorful.hrm.util.AjaxResult;

public class BaiduAIUtil {
    public static AjaxResult autditTxtContent(String content){
        //通过百度人工智能审核的接口去调用-就是通过httpclient发送http请求就得到结果，一般在公司里面都是一个工具类
        if (content.contains("dbl")) {
            return AjaxResult.me().setSuccess(false).setMessage("不能输入违规文本！");
        }
        return AjaxResult.me();
    }

    public static AjaxResult autditAudioContent(String audioPath){
        //通过百度人工智能审核的接口去调用-就是通过httpclient发送http请求就得到结果，一般在公司里面都是一个工具类
        return AjaxResult.me();
    }
    public static AjaxResult autditVideoContent(String videoPath){
        //通过百度人工智能审核的接口去调用-就是通过httpclient发送http请求就得到结果，一般在公司里面都是一个工具类
        return AjaxResult.me();
    }
}
