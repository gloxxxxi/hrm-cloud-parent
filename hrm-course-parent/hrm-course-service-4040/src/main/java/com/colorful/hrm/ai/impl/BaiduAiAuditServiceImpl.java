package com.colorful.hrm.ai.impl;

import com.colorful.hrm.ai.IAIContentAuditService;
import com.colorful.hrm.ai.util.BaiduAIUtil;
import com.colorful.hrm.util.AjaxResult;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class BaiduAiAuditServiceImpl implements IAIContentAuditService {
    @Override
    public AjaxResult audit(String content, String auditAudioPath, String auditVideoPath) {

        //通过百度人工智能审核的接口去调用-就是通过httpclient发送http请求就得到结果，一般在公司里面都是一个工具类
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(content)) {
            AjaxResult ajaxResult = BaiduAIUtil.autditTxtContent(content);
            if (!ajaxResult.isSuccess()){
                sb.append("文本校验错误，具体错误如下：");
                sb.append(ajaxResult.getMessage());
            }
        }

        if (StringUtils.isNotEmpty(auditAudioPath)) {
            AjaxResult ajaxResult = BaiduAIUtil.autditAudioContent(auditAudioPath);
            if (!ajaxResult.isSuccess()){
                sb.append("音频校验错误，具体错误如下：");
                sb.append(ajaxResult.getMessage());
            }

        }
        if (StringUtils.isNotEmpty(auditVideoPath)) {
            AjaxResult ajaxResult = BaiduAIUtil.autditVideoContent(auditVideoPath);
            if (!ajaxResult.isSuccess()){
                sb.append("视频频校验错误，具体错误如下：");
                sb.append(ajaxResult.getMessage());
            }
        }

        //根据message来判断是审核通过或者审核失败
        String message = sb.toString();
        if (!StringUtils.isEmpty(message))
            return AjaxResult.me().setSuccess(false).setMessage(message);
        return AjaxResult.me();
    }
}
