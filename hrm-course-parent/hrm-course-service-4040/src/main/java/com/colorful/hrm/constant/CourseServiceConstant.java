package com.colorful.hrm.constant;

/**
 * @author SuperJellyfish
 * @description 课程服务常量
 * @date 2022/1/6 19:06
 */
public class CourseServiceConstant {

    /** 缓存中课程类型树形结构 */
    public static final String COURSE_TYPE_TREE = "courseType_treeData_in_cache";
}
