package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.CourseMarket;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface CourseMarketMapper extends BaseMapper<CourseMarket> {

}
