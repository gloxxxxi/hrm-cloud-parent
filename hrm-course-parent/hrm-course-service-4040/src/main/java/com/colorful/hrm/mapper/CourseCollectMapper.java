package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.CourseCollect;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品收藏 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface CourseCollectMapper extends BaseMapper<CourseCollect> {

}
