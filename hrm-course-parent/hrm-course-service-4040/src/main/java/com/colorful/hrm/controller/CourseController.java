package com.colorful.hrm.controller;

import com.colorful.hrm.dto.CourseDto;
import com.colorful.hrm.service.ICourseService;
import com.colorful.hrm.entity.Course;
import com.colorful.hrm.query.CourseQuery;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {
    @Autowired
    public ICourseService courseService;


    /**
     * 保存和修改公用的
     * @param courseDto  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody @Valid CourseDto courseDto){
        try {
            if( courseDto.getCourse().getId()!=null) {
                courseService.updateById(courseDto);
            } else {
                courseService.insert(courseDto);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            courseService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }

    //获取用户
    @PreAuthorize("hasAuthority('course:getById')")
    @GetMapping("/{id}")
    public Course get(@PathVariable("id")Long id)
    {
        return courseService.selectById(id);
    }


    /**
     * 查看所有的员工信息
     * @return
     */
    @GetMapping
    @PreAuthorize("hasAuthority('course:get')")
    public List<Course> list(){

        return courseService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<Course> json(@RequestBody CourseQuery query)
    {

        return courseService.selectByPage(query);
    }

    @PreAuthorize("hasAuthority('course:online')")
    @PostMapping("/online") //上线
    public AjaxResult online(@RequestBody List<Long> ids)
    {
        return courseService.online(ids);
    }

    //下线
    @PostMapping("/offline")
    @PreAuthorize("hasAuthority('course:offline')")
    public AjaxResult offline(@RequestBody List<Long> ids)
    {
        return courseService.offline(ids);
    }
}
