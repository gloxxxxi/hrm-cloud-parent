package com.colorful.hrm.service;

import com.colorful.hrm.entity.CourseType;
import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.vo.CrumbsVo;

import java.util.List;

/**
 * <p>
 * 课程目录 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface ICourseTypeService extends IService<CourseType> {

    /**
     * 获取课程树
     * @param pid 父节点id
     * @return 课程类型集合
     */
    List<CourseType> getTree(Long pid);

    List<CrumbsVo> queryCrumbs(Long typeId);
}
