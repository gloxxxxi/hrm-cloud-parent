package com.colorful.hrm.vo;


import com.colorful.hrm.entity.CourseType;

import java.util.ArrayList;
import java.util.List;

//用来封装面包屑
public class CrumbsVo {

    //自己个
    private CourseType ownerCourseType;

    //兄弟姐妹
    private List<CourseType> otherCourseTypes = new ArrayList<>();

    public CourseType getOwnerCourseType() {
        return ownerCourseType;
    }

    public void setOwnerCourseType(CourseType ownerCourseType) {
        this.ownerCourseType = ownerCourseType;
    }

    public List<CourseType> getOtherCourseTypes() {
        return otherCourseTypes;
    }

    public void setOtherCourseTypes(List<CourseType> otherCourseTypes) {
        this.otherCourseTypes = otherCourseTypes;
    }
}