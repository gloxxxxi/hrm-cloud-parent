package com.colorful.hrm.enums;

public enum  CourseGrade {
    BlackIron("黑铁",1),
    Bronze("青铜",2),
    Silver("白银",3),
    Platinum("铂金",4);

    //等级名称
    private String gradeName;
    //等级id
    private int gradeId;


    //静态方法
    public static CourseGrade getGrade(int gradeId){
        //获取所有的枚举值
        CourseGrade[] values = CourseGrade.values();
        //遍历判断是否在里面
        for (CourseGrade grade : values) {
            int gradeId1 = grade.getGradeId();
            if (gradeId==gradeId1)
                return  grade;
        }
        return null;
    }

    CourseGrade(String gradeName, int gradeId) {
        this.gradeName = gradeName;
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    @Override
    public String toString() {
        return "CourseGrade{" +
                "gradeName='" + gradeName + '\'' +
                ", gradeId=" + gradeId +
                '}';
    }
}