package com.colorful.hrm.ai;


import com.colorful.hrm.util.AjaxResult;

//人工智能内容审核
public interface IAIContentAuditService {

    /**
     *  审核文本，声音，视频三合一接口，如果某一个审核传入null
     * @param content 文本
     * @param auditAudioPath 声音路径
     * @param auditVideoPath 视频路径
     * @return
     */
    AjaxResult audit(String content, String auditAudioPath, String  auditVideoPath);
}
