package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.CourseDetail;
import com.colorful.hrm.mapper.CourseDetailMapper;
import com.colorful.hrm.service.ICourseDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
@Service
public class CourseDetailServiceImpl extends ServiceImpl<CourseDetailMapper, CourseDetail> implements ICourseDetailService {

}
