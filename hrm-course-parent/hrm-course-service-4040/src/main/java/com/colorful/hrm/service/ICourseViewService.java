package com.colorful.hrm.service;

import com.colorful.hrm.entity.CourseView;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品浏览 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface ICourseViewService extends IService<CourseView> {

}
