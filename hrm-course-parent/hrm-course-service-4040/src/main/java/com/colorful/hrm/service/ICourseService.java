package com.colorful.hrm.service;

import com.colorful.hrm.dto.CourseDto;
import com.colorful.hrm.entity.Course;
import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.query.CourseQuery;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface ICourseService extends IService<Course> {

    PageList<Course> selectByPage(CourseQuery query);

    void updateById(CourseDto courseDto);

    void insert(CourseDto courseDto);

    AjaxResult online(List<Long> ids);

    AjaxResult offline(List<Long> ids);
}
