package com.colorful.hrm.service;

import com.colorful.hrm.entity.CourseCollect;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品收藏 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface ICourseCollectService extends IService<CourseCollect> {

}
