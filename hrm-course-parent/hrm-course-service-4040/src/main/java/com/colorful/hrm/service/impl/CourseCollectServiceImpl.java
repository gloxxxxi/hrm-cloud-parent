package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.CourseCollect;
import com.colorful.hrm.mapper.CourseCollectMapper;
import com.colorful.hrm.service.ICourseCollectService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品收藏 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
@Service
public class CourseCollectServiceImpl extends ServiceImpl<CourseCollectMapper, CourseCollect> implements ICourseCollectService {

}
