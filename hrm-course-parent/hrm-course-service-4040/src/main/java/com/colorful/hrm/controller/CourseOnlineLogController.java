package com.colorful.hrm.controller;

import com.colorful.hrm.service.ICourseOnlineLogService;
import com.colorful.hrm.entity.CourseOnlineLog;
import com.colorful.hrm.query.CourseOnlineLogQuery;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/courseOnlineLog")
public class CourseOnlineLogController {
    @Autowired
    public ICourseOnlineLogService courseOnlineLogService;


    /**
     * 保存和修改公用的
     * @param courseOnlineLog  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody CourseOnlineLog courseOnlineLog){
        try {
            if( courseOnlineLog.getId()!=null) {
                courseOnlineLogService.updateById(courseOnlineLog);
            } else {
                courseOnlineLogService.insert(courseOnlineLog);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            courseOnlineLogService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public CourseOnlineLog get(@PathVariable("id")Long id)
    {
        return courseOnlineLogService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public List<CourseOnlineLog> list(){

        return courseOnlineLogService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<CourseOnlineLog> json(@RequestBody CourseOnlineLogQuery query)
    {
        Page<CourseOnlineLog> page = new Page<CourseOnlineLog>(query.getPage(),query.getRows());
        page = courseOnlineLogService.selectPage(page);
        return new PageList<CourseOnlineLog>(page.getTotal(),page.getRecords());
    }
}
