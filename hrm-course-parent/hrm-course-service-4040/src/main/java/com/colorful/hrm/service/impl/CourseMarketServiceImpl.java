package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.CourseMarket;
import com.colorful.hrm.mapper.CourseMarketMapper;
import com.colorful.hrm.service.ICourseMarketService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
@Service
public class CourseMarketServiceImpl extends ServiceImpl<CourseMarketMapper, CourseMarket> implements ICourseMarketService {

}
