package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.CourseOnlineLog;
import com.colorful.hrm.mapper.CourseOnlineLogMapper;
import com.colorful.hrm.service.ICourseOnlineLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-08
 */
@Service
public class CourseOnlineLogServiceImpl extends ServiceImpl<CourseOnlineLogMapper, CourseOnlineLog> implements ICourseOnlineLogService {

}
