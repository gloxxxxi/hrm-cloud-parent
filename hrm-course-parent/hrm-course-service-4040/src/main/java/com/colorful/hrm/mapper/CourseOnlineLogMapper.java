package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.CourseOnlineLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-08
 */
public interface CourseOnlineLogMapper extends BaseMapper<CourseOnlineLog> {

    void batchSave(List<CourseOnlineLog> courseOnlineLogs);
}
