package com.colorful.hrm.config;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Mybatis-Plus 分页器
 *              // @EnableTransactionManagement 开启事务支持
 * @author SuperJellyfish
 */
@EnableTransactionManagement
@Configuration
public class MybatisPlusConfig {
 /**
 * 分页插件
 */
 @Bean
 public PaginationInterceptor paginationInterceptor() {
 return new PaginationInterceptor();
 }
}