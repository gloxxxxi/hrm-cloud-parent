package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.CourseView;
import com.colorful.hrm.mapper.CourseViewMapper;
import com.colorful.hrm.service.ICourseViewService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品浏览 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
@Service
public class CourseViewServiceImpl extends ServiceImpl<CourseViewMapper, CourseView> implements ICourseViewService {

}
