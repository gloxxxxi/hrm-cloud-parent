package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.CourseAuditLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-08
 */
public interface CourseAuditLogMapper extends BaseMapper<CourseAuditLog> {

}
