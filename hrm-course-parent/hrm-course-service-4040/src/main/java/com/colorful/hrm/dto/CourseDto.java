package com.colorful.hrm.dto;


import com.colorful.hrm.entity.Course;
import com.colorful.hrm.entity.CourseDetail;
import com.colorful.hrm.entity.CourseMarket;

public class CourseDto {
    private Course course;
    private CourseMarket courseMarket;
    private CourseDetail courseDetail;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public CourseMarket getCourseMarket() {
        return courseMarket;
    }

    public void setCourseMarket(CourseMarket courseMarket) {
        this.courseMarket = courseMarket;
    }

    public CourseDetail getCourseDetail() {
        return courseDetail;
    }

    public void setCourseDetail(CourseDetail courseDetail) {
        this.courseDetail = courseDetail;
    }
}