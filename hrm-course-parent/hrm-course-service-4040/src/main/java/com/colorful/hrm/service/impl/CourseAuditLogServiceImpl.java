package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.CourseAuditLog;
import com.colorful.hrm.mapper.CourseAuditLogMapper;
import com.colorful.hrm.service.ICourseAuditLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-08
 */
@Service
public class CourseAuditLogServiceImpl extends ServiceImpl<CourseAuditLogMapper, CourseAuditLog> implements ICourseAuditLogService {

}
