package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.CourseView;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品浏览 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-06
 */
public interface CourseViewMapper extends BaseMapper<CourseView> {

}
