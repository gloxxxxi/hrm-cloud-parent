package com.colorful.hrm.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-08
 */
@TableName("t_course_audit_log")
public class CourseAuditLog extends Model<CourseAuditLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("course_id")
    private Long courseId;
    @TableField("course_name")
    private String courseName;
    /**
     * 0 失败 1 通过
     */
    private Integer state;
    /**
     * 备注
     */
    private String note;
    /**
     * 审核人id，如果是自动审核，直接不写
     */
    @TableField("audit_admin_id")
    private Long auditAdminId;
    @TableField("audit_admin_name")
    private String auditAdminName;
    @TableField("audit_time")
    private Date auditTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getAuditAdminId() {
        return auditAdminId;
    }

    public void setAuditAdminId(Long auditAdminId) {
        this.auditAdminId = auditAdminId;
    }

    public String getAuditAdminName() {
        return auditAdminName;
    }

    public void setAuditAdminName(String auditAdminName) {
        this.auditAdminName = auditAdminName;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CourseAuditLog{" +
        ", id=" + id +
        ", courseId=" + courseId +
        ", courseName=" + courseName +
        ", state=" + state +
        ", note=" + note +
        ", auditAdminId=" + auditAdminId +
        ", auditAdminName=" + auditAdminName +
        ", auditTime=" + auditTime +
        "}";
    }
}
