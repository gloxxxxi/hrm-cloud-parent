package com.colorful.hrm.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-08
 */
@TableName("t_course_online_log")
public class CourseOnlineLog extends Model<CourseOnlineLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("course_id")
    private Long courseId;
    @TableField("course_name")
    private String courseName;
    /**
     * 0 下线 1 上线
     */
    private Integer state;
    @TableField("opr_admin_id")
    private Long oprAdminId;
    @TableField("opr_admin_name")
    private String oprAdminName;
    @TableField("online_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date onlineTime;
    @TableField("offline_time")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date offlineTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getOprAdminId() {
        return oprAdminId;
    }

    public void setOprAdminId(Long oprAdminId) {
        this.oprAdminId = oprAdminId;
    }

    public String getOprAdminName() {
        return oprAdminName;
    }

    public void setOprAdminName(String oprAdminName) {
        this.oprAdminName = oprAdminName;
    }

    public Date getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    public Date getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Date offlineTime) {
        this.offlineTime = offlineTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CourseOnlineLog{" +
        ", id=" + id +
        ", courseId=" + courseId +
        ", courseName=" + courseName +
        ", state=" + state +
        ", oprAdminId=" + oprAdminId +
        ", oprAdminName=" + oprAdminName +
        ", onlineTime=" + onlineTime +
        ", offlineTime=" + offlineTime +
        "}";
    }
}
