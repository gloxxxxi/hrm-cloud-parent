# 1. HRM-人力资源管理系统

## 1.1. 项目概述

基于现在想找工作的人无技能，招聘公司找不到合适的人，培训机构招不到人的现状，我司就研发了SAAS ,HRM项目。首先让培训机构、招聘公司等入驻到我们平台，可以进行获客或招聘，然后用户就可以选择一些培训机构参与学习，学习完成后就可以在本平台投递简历找工作。本台可以收取入住费，服务费、广告费进行营利。本项目整体使用前后端分离架构，后端使用微服务架构，前端管理端使用vue技术栈，主站使用 html + css.

## 1.2. SAAS

SAAS：软件即服务

PAAS：平台即服务

IAAS：基础设施即服务

## 1.3. 问题

1. 数据量大 - 做数据库优化
2. 需要做数据隔离
   1. 创建独立数据库服务器
   2. 同一个数据库服务器，不同的数据库
   3. 同一个数据库，不同的表
   4. 同一个表通过外键区分
3. 权限控制
   1. Department-Employee-Role-Permission-Menu
   2. TenantType-Tenant-Meal-Permission-Menu
   3. 微服务中可以不用数据字典，直接在前端写死，修改就发布新版本

## 1.4. 项目开发流程

1. 立项：接项目，项目经理或产品经理
2. 需求分析：项目经理或产品经理
3. 项目启动：
   1. 组建团队
      1. PM 项目经理 
      2. SE 架构师
      3. 开发
         1. 前端
         2. 后端
      4. 测试
      5. 运维
   2. 技术架构与技术选型和基础模型搭建SE
4. 多次迭代开发
   1. 开发：前后端并行开发
   2. 测试
   3. 上线
5. 进入运维：留个1-2人维护。其他人转入其他项目

## 1.5. 技术架构和技术栈：前后端分离

1. 前端
   1. 管理端：VUE技术栈
   2. 用户站点：HTML + CSS
2. 后端
   1. SpringCloud
   2. SpringBoot
   3. SpringMVC
   4. Spring
   5. MyBatis-Plus
   6. ES
   7. RabbitMQ
   8. Aliyun OSS
   9. Docker
   10. k8s

# 2. 项目原型搭建

## 2.1. hrm-parent

项目结构

![image-20211230185923171](.\img\image-20211230185923171.png)

### 2.1.1. hrm-basic-parent

基础模块

​	-- 	hrm-basic-common 存放公共代码

​	--	query、util

### 2.1.2. hrm-support-parent

支持模块

- config 统一配置中心
- eureka 注册中心
- zuul 网关

### 2.1.3. 众多微服务

- client
- common
- service

### 2.1.4. hrm-test-parent

测试模块

- mybatis-plus-parent
  - 代码生成模块
  - 代码生成器模块
- 其他测试

## 2.2. 上传远程仓库

移除原有的仓库

Setting - Version Control 移除原有仓库

VCS -> Enable Version Control

Terminal 

```shell
git pull 远程仓库 --allow-unrelated-histories
```

Add -> Commit -> Push



# 3. MyBatis-Plus

## 代码生成器模块

pom.xml

```xml
<dependency>
 <groupId>com.baomidou</groupId>
 <artifactId>mybatis-plus-boot-starter</artifactId>
 <version>2.2.0</version>
 </dependency>
 <!--模板引擎-->
 <dependency>
 <groupId>org.apache.velocity</groupId>
 <artifactId>velocity-engine-core</artifactId>
 <version>2.0</version>
 </dependency>
 <dependency>
 <groupId>mysql</groupId>
 <artifactId>mysql-connector-java</artifactId>
 </dependency>
```

配置文件

```properties
#此处为本项目src所在路径（代码生成器输出路径）,注意一定是当前项目所在的目录
OutputDir=D:\\project-demo\\hrm-cloud-parent\\hrm-test-parent\\mybtis-plus-parent\\user-service\\src\\main\\java
#mapper.xml SQL映射文件目录
OutputDirXml=D:\\project-demo\\hrm-cloud-parent\\hrm-test-parent\\mybtis-plus-parent\\user-service\\src\\main\\resources
#feign放那儿
OutputDirBase=D:\\project-demo\\hrm-cloud-parent\\hrm-test-parent\\mybtis-plus-parent\\user-service\\src\\main\\java
#设置作者
author=super-jellyfish
#自定义包路径
parent=com.colorful.hrm

#数据库连接信息
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql:///hrm_user
jdbc.user=root
jdbc.pwd=root
```

配置模板

resources/templates

1.  [client.java.vm](resouces\templates\client.java.vm) 
2.  [ClientHystrixFallbackFactory.java.vm](resouces\templates\ClientHystrixFallbackFactory.java.vm) 
3.  [controller.java.vm](resouces\templates\controller.java.vm) 
4.  [query.java.vm](resouces\templates\query.java.vm) 

生成器 GenteratorCode.java

```java
package com.colorful;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.*;

/**
 * Created by CDHong on 2018/4/6.
 */
public class GenteratorCode {

    public static void main(String[] args) throws InterruptedException {
        //用来获取Mybatis-Plus.properties文件的配置信息
        ResourceBundle rb = ResourceBundle.getBundle("mp-config-user"); //不要加后缀
//        ResourceBundle rb = ResourceBundle.getBundle("mp-config-order"); //不要加后缀
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(rb.getString("OutputDir"));
        gc.setFileOverride(true);
        gc.setActiveRecord(true);// 开启 activeRecord 模式
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(false);// XML columList
        gc.setAuthor(rb.getString("author"));
        mpg.setGlobalConfig(gc);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setTypeConvert(new MySqlTypeConvert());
        dsc.setDriverName(rb.getString("jdbc.driver"));
        dsc.setUsername(rb.getString("jdbc.user"));
        dsc.setPassword(rb.getString("jdbc.pwd"));
        dsc.setUrl(rb.getString("jdbc.url"));
        mpg.setDataSource(dsc);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setTablePrefix(new String[] { "t_" });// 此处可以修改为您的表前缀
        strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略
//        strategy.setInclude(new String[]{"t_order"}); // 需要生成的表
        strategy.setInclude(new String[]{"t_vip_user"}); // 需要生成的表
        mpg.setStrategy(strategy);
        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(rb.getString("parent"));
        pc.setController("controller");
        pc.setService("service");
        pc.setServiceImpl("service.impl");
        pc.setEntity("domain");
        pc.setMapper("mapper");
        mpg.setPackageInfo(pc);

        // 注入自定义配置，可以在 VM 中使用 cfg.abc 【可无】
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-rb");
                this.setMap(map);
            }
        };

        List<FileOutConfig> focList = new ArrayList<FileOutConfig>();

        // 调整 domain 生成目录演示
        focList.add(new FileOutConfig("/templates/entity.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return rb.getString("OutputDirBase")+ "/com/colorful/hrm/domain/" + tableInfo.getEntityName() + ".java";
            }
        });
        //query
        focList.add(new FileOutConfig("/templates/query.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return rb.getString("OutputDirBase")+ "/com/colorful/hrm/query/" + tableInfo.getEntityName() + "Query.java";
            }
        });

        // 调整 xml 生成目录演示
        focList.add(new FileOutConfig("/templates/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return rb.getString("OutputDirXml")+ "/com/colorful/hrm/mapper/" + tableInfo.getEntityName() + "Mapper.xml";
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
        // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
        TemplateConfig tc = new TemplateConfig();
        tc.setService("/templates/service.java.vm");
        tc.setServiceImpl("/templates/serviceImpl.java.vm");
        tc.setEntity(null);
        tc.setMapper("/templates/mapper.java.vm");
        //tc.setController(null);
        tc.setXml(null);
        // 如上任何一个模块如果设置 空 OR Null 将不生成该模块。
        mpg.setTemplate(tc);

        // 执行生成
        mpg.execute();
    }

}
```

## 生成代码模块

pom.xml

```java
<dependency>
 <groupId>com.baomidou</groupId>
 <artifactId>mybatis-plus-boot-starter</artifactId>
 <version>2.2.0</version>
 </dependency>
 <dependency>
 <groupId>mysql</groupId>
 <artifactId>mysql-connector-java</artifactId>
 </dependency>
<dependency>
 <groupId>org.springframework.boot</groupId>
 <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
 <groupId>org.springframework.boot</groupId>
 <artifactId>spring-boot-starter-test</artifactId>
 
</dependency>
```

application.yml

```properties
server:
  port: 8888
spring:
  application:
    name: user-service-test
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/hrm_user
    username: root
    password: root
mybatis-plus:
  type-aliases-package: com.colorful.hrm.domain,com.colorful.hrm.query
```

启动类

```java
@SpringBootApplication
@MapperScan("com.colorful.hrm.mapper")
public class UserServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }
}

```

