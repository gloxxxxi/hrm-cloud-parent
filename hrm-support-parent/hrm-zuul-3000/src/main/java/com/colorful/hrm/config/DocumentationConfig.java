package com.colorful.hrm.config;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * //选项及跳转而皮脂
 * @author 11695
 */
@Component
@Primary
public class DocumentationConfig implements SwaggerResourcesProvider {
    @Override
    public List<SwaggerResource> get() {
        List resources = new ArrayList<>();
        resources.add(swaggerResource("系统中心", "/services/sysmanage/v2/api-docs", "2.0"));
        resources.add(swaggerResource("授权中心", "/services/auth/v2/api-docs", "2.0"));
        resources.add(swaggerResource("阿里云OSS中心", "/services/aliyunoss/v2/api-docs", "2.0"));
        resources.add(swaggerResource("课程中心", "/services/course/v2/api-docs", "2.0"));
        resources.add(swaggerResource("搜索中心", "/services/search/v2/api-docs", "2.0"));
        resources.add(swaggerResource("第三方中心", "/services/thirdparty/v2/api-docs", "2.0"));
        resources.add(swaggerResource("用户中心", "/services/user/v2/api-docs", "2.0"));
        resources.add(swaggerResource("秒杀中心", "/services/kill/v2/api-docs", "2.0"));
        return resources;

    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}