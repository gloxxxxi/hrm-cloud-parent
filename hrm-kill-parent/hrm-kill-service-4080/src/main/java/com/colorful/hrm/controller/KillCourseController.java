package com.colorful.hrm.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.colorful.hrm.domain.KillCourse;
import com.colorful.hrm.query.KillCourseQuery;
import com.colorful.hrm.service.IKillCourseService;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/killCourse")
public class KillCourseController {
    @Autowired
    public IKillCourseService killCourseService;


    /**
     * 保存和修改公用的
     * @param killCourse  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody KillCourse killCourse){
        try {
            if( killCourse.getId()!=null)
                killCourseService.updateById(killCourse);
            else
                killCourseService.insert(killCourse);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            killCourseService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public KillCourse get(@PathVariable("id")Long id)
    {
        return killCourseService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public List<KillCourse> list(){

        return killCourseService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<KillCourse> json(@RequestBody KillCourseQuery query)
    {
        Page<KillCourse> page = new Page<KillCourse>(query.getPage(),query.getRows());
        page = killCourseService.selectPage(page);
        return new PageList<KillCourse>(page.getTotal(),page.getRecords());
    }
}
