package com.colorful.hrm.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.colorful.hrm.domain.KillCourseLog;
import com.colorful.hrm.mapper.KillCourseLogMapper;
import com.colorful.hrm.service.IKillCourseLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yaosang
 * @since 2022-01-20
 */
@Service
public class KillCourseLogServiceImpl extends ServiceImpl<KillCourseLogMapper, KillCourseLog> implements IKillCourseLogService {

}
