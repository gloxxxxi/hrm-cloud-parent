package com.colorful.hrm.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.colorful.hrm.domain.KillCourseLog;
import com.colorful.hrm.query.KillCourseLogQuery;
import com.colorful.hrm.service.IKillCourseLogService;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/killCourseLog")
public class KillCourseLogController {
    @Autowired
    public IKillCourseLogService killCourseLogService;


    /**
     * 保存和修改公用的
     * @param killCourseLog  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody KillCourseLog killCourseLog){
        try {
            if( killCourseLog.getId()!=null)
                killCourseLogService.updateById(killCourseLog);
            else
                killCourseLogService.insert(killCourseLog);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            killCourseLogService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public KillCourseLog get(@PathVariable("id")Long id)
    {
        return killCourseLogService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public List<KillCourseLog> list(){

        return killCourseLogService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<KillCourseLog> json(@RequestBody KillCourseLogQuery query)
    {
        Page<KillCourseLog> page = new Page<KillCourseLog>(query.getPage(),query.getRows());
        page = killCourseLogService.selectPage(page);
        return new PageList<KillCourseLog>(page.getTotal(),page.getRecords());
    }
}
