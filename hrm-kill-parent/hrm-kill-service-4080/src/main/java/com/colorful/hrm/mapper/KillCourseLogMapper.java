package com.colorful.hrm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.colorful.hrm.domain.KillCourseLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yaosang
 * @since 2022-01-20
 */
public interface KillCourseLogMapper extends BaseMapper<KillCourseLog> {

}
