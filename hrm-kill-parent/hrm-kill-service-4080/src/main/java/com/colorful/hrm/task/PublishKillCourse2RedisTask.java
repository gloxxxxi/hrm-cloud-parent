package com.colorful.hrm.task;

import com.colorful.hrm.service.IKillCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class PublishKillCourse2RedisTask {
    @Autowired
    private IKillCourseService killCourseService;
    @Scheduled(cron = "*/10 * * * * ?") //cron表达式
    public void publishKillCourse2Redis(){
    //System.out.println(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        killCourseService.publishKillCourse2Redis();
    }
}
