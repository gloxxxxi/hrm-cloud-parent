package com.colorful.hrm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.colorful.hrm.mapper")
@EnableScheduling // 开启定时任务功能
public class KillServerApp {
    public static void main(String[] args) {
        SpringApplication.run(KillServerApp.class,args);
    }
}
