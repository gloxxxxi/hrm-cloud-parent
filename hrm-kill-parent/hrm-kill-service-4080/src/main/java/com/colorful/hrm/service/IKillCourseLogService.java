package com.colorful.hrm.service;

import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.domain.KillCourseLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yaosang
 * @since 2022-01-20
 */
public interface IKillCourseLogService extends IService<KillCourseLog> {

}
