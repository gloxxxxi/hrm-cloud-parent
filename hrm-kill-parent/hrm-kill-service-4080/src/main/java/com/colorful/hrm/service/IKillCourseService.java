package com.colorful.hrm.service;

import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.domain.KillCourse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yaosang
 * @since 2022-01-20
 */
public interface IKillCourseService extends IService<KillCourse> {

    void publishKillCourse2Redis();
}
