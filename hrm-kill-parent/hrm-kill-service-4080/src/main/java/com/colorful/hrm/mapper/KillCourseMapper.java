package com.colorful.hrm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.colorful.hrm.domain.KillCourse;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yaosang
 * @since 2022-01-20
 */
public interface KillCourseMapper extends BaseMapper<KillCourse> {

}
