package com.colorful.hrm.domain;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yaosang
 * @since 2022-01-20
 */
@TableName("t_kill_course_log")
public class KillCourseLog extends Model<KillCourseLog> {

    private static final long serialVersionUID = 1L;

    @TableId("course_id")
    private Long courseId;
    private Long id;
    @TableField("screenings_id")
    private Long screeningsId;
    @TableField("course_name")
    private String courseName;


    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getScreeningsId() {
        return screeningsId;
    }

    public void setScreeningsId(Long screeningsId) {
        this.screeningsId = screeningsId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Override
    protected Serializable pkVal() {
        return this.courseId;
    }

    @Override
    public String toString() {
        return "KillCourseLog{" +
        ", courseId=" + courseId +
        ", id=" + id +
        ", screeningsId=" + screeningsId +
        ", courseName=" + courseName +
        "}";
    }
}
