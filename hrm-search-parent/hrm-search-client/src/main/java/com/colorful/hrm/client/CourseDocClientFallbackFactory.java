package com.colorful.hrm.client;

import com.colorful.hrm.doc.CourseDoc;
import com.colorful.hrm.util.AjaxResult;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CourseDocClientFallbackFactory  implements FallbackFactory<CourseDocClient> {
    //打日志
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public CourseDocClient create(Throwable throwable) {
        return new CourseDocClient() {
            @Override
            public AjaxResult batchAdd(List<CourseDoc> courseDocs) {
                throwable.printStackTrace();
                logger.error("批量添加失败！系统异常，请联系管理员");
                return AjaxResult.me().setSuccess(false).setMessage("批量添加失败！系统异常，请联系管理员");
            }

            @Override
            public AjaxResult batchDel(List<Long> ids) {
                throwable.printStackTrace();
                logger.error("批量删除失败！{}",ids);
                return AjaxResult.me().setSuccess(false).setMessage("批量删除失败！系统异常，请联系管理员");
            }
        };
    }
}
