package com.colorful.hrm.client;

import com.colorful.hrm.doc.CourseDoc;
import com.colorful.hrm.util.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author 11695
 */
@RequestMapping("/courseDoc")
@FeignClient(value = "SEARCH-SERVICE",fallbackFactory =CourseDocClientFallbackFactory.class)
public interface CourseDocClient {
    //批量添加
    @PostMapping("/add")
    AjaxResult batchAdd(@RequestBody List<CourseDoc> courseDocs);

    //批量删除
    @PostMapping("/del")
    AjaxResult batchDel(@RequestBody List<Long> ids);
}
