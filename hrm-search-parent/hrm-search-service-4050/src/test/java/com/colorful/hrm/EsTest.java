package com.colorful.hrm;

import com.colorful.hrm.doc.CourseDoc;
import com.colorful.hrm.doc.EmployeeDoc;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchServerApp.class)
public class EsTest {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    //索引库，文档映射
    @Test
    public void preHandle() throws Exception{
        elasticsearchTemplate.createIndex(CourseDoc.class);//创建索引库
        elasticsearchTemplate.putMapping(CourseDoc.class);//创建类型映射
    }


}
