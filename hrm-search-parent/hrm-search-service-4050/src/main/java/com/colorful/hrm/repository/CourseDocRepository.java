package com.colorful.hrm.repository;

import com.colorful.hrm.doc.CourseDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseDocRepository  extends ElasticsearchRepository<CourseDoc,Long> {
}
