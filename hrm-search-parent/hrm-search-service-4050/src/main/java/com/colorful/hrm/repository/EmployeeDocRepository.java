package com.colorful.hrm.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;


//spring data es会根据接口产生动态代理对象，并且放入spring，我们只需要注入即可使用
//@Repository
//public interface EmployeeDocRepository extends ElasticsearchRepository<EmployeeDoc,Long> {
//}
