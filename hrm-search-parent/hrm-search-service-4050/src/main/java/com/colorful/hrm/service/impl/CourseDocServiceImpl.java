package com.colorful.hrm.service.impl;

import com.colorful.hrm.config.HighlightResultMapper;
import com.colorful.hrm.doc.CourseDoc;
import com.colorful.hrm.query.CourseDocQuery;
import com.colorful.hrm.repository.CourseDocRepository;
import com.colorful.hrm.service.ICourseDocService;
import com.colorful.hrm.util.AggsVo;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 11695
 */
@Service
public class CourseDocServiceImpl implements ICourseDocService {
    @Autowired
    private CourseDocRepository repository;

    @Autowired
    private ElasticsearchTemplate template;
    @Override
    public AjaxResult batchAdd(List<CourseDoc> courseDocs) {
        System.out.println("online .....................................");
        repository.saveAll(courseDocs);
        return AjaxResult.me();
    }

    @Override
    public AjaxResult batchDel(List<Long> ids) {
        Iterable<CourseDoc> allById = repository.findAllById(ids);
        System.out.println("offline .....................................");
        repository.deleteAll(allById);
        return AjaxResult.me();
    }


    @Autowired
    private HighlightResultMapper highlightResultMapper;

    /**
     * {
     * "query": {
     *    "bool": {  //booleanQuery 组合查询
     *           "must": [   //查询 与(must) 或(should) 非(must not)
     * {
     * "match": {//标准查询(分词匹配)  term：单词查询(部分词)
     * "description": "hello java"
     * }
     * }
     * ],
     *            "filter": {  //过滤
     *                "term": {"description": "hello world"}
     *            }
     *     }
     * },
     * "from": 20,
     * "size": 10,
     * "_source": ["fullName", "age", "email"],
     * "sort": [{"join_date": "desc"},{"age": "asc"}]
     * }
     * @param query
     * @return
     */
    @Override
    public PageList<CourseDoc> queryPageData(CourseDocQuery query) {
        //1创建构建器
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();
        //2 设置查询条件
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        String keyword = query.getKeyword();
        //2.1 dsl查询
        if (!StringUtils.isEmpty(keyword)){
            boolQuery.must(QueryBuilders.multiMatchQuery(keyword,"name","forUser","gradeName","tenantName","courseTypeName"));
//            boolQuery.must(QueryBuilders.multiMatchQuery(keyword,"name","forUser","gradeName"));
//            boolQuery.must(QueryBuilders.matchQuery("all",keyword));
        }
        //2.2 dsl过滤  类型 机构 minprie maxprice
        Long courseTypeId = query.getCourseTypeId();
        if (courseTypeId!=null){
            boolQuery.filter(QueryBuilders.termQuery("courseTypeId",courseTypeId));
        }
        Long tenantId = query.getTenantId();
        if (tenantId!=null){
            boolQuery.filter(QueryBuilders.termQuery("tenantId",tenantId));
        }
        Integer priceMin = query.getPriceMin();
        Integer priceMax = query.getPriceMax();
        if (priceMin!=null && priceMax!=null){
            boolQuery.filter(QueryBuilders.rangeQuery("price").gte(priceMin).lte(priceMax));
        }
        builder.withQuery(boolQuery);
        //3 排序
        String sortField = query.getSortField();
        String sortFiledName = "price"; //默认按照价格
        switch (sortField.toLowerCase()){
            case "xL": sortFiledName="saleCount";break;
            case "xp": sortFiledName="onlineTime";break;
            case "pl": sortFiledName="commentCount";break;
            case "rq": sortFiledName="viewCount";break;
        }
        SortOrder defalutSortOrder = SortOrder.DESC; //默认是降序
        if (!query.getSortType().equals("desc")){ //如果不是降序就设置为升序
            defalutSortOrder = SortOrder.ASC;
        }
        builder.withSort(SortBuilders.fieldSort(sortFiledName).order(defalutSortOrder));
        //4 分页
        builder.withPageable(PageRequest.of(query.getPage()-1,query.getRows()));

        // 5 高亮查询
        HighlightBuilder.Field field = new HighlightBuilder.Field("name")
                .preTags("<b style='color:red'>")
                .postTags("</b>");
        builder.withHighlightFields(field);

        //6 聚合处理  - size() 查询精准度
        builder.addAggregation(AggregationBuilders.terms("tenantNameAggs").field("tenantName.keyword").size(20));
        builder.addAggregation(AggregationBuilders.terms("courseTypeNameAggs").field("courseTypeName.keyword").size(20));
        //5 封装结果
//        Page<CourseDoc> page = repository.search(builder.build());
        AggregatedPage<CourseDoc> page = template.queryForPage(builder.build(), CourseDoc.class,highlightResultMapper);
        HashMap<String, List<AggsVo>> aggs = new HashMap<>();
        StringTerms tenantNameAggs = (StringTerms) page.getAggregation("tenantNameAggs");
        List<AggsVo> tenantNameAggsVos = getAggsVos(tenantNameAggs);
        aggs.put("tenantNameAggs",tenantNameAggsVos);
        StringTerms courseTypeNameAggs = (StringTerms) page.getAggregation("courseTypeNameAggs");
        List<AggsVo> courseTypeNameAggsVos = getAggsVos(courseTypeNameAggs);
        aggs.put("courseTypeNameAggs",courseTypeNameAggsVos);

        if (page.getTotalElements()==0) {
            return new PageList();
        } else{
            PageList<CourseDoc> pageList = new PageList<>(page.getTotalElements(), page.getContent());
            pageList.setAggsVoData(aggs);
            return pageList;
        }
    }

    private List<AggsVo> getAggsVos(StringTerms terms) {
        List<StringTerms.Bucket> buckets = terms.getBuckets();
        List<AggsVo> aggsVos = buckets.stream().map(bucket -> {
            AggsVo aggsVo = new AggsVo();
            String name = bucket.getKeyAsString();
            long count = bucket.getDocCount();
            aggsVo.setName(name);
            aggsVo.setCount(count);
            return aggsVo;
        }).collect(Collectors.toList());

        return aggsVos;
    }
}
