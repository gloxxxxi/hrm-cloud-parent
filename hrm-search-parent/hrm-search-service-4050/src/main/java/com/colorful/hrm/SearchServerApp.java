package com.colorful.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SearchServerApp {
    public static void main(String[] args) {
        SpringApplication.run(SearchServerApp.class,args);
    }
}
