package com.colorful.hrm.service;

import com.colorful.hrm.doc.CourseDoc;
import com.colorful.hrm.query.CourseDocQuery;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;

import java.util.List;

public interface ICourseDocService {
    AjaxResult batchAdd(List<CourseDoc> courseDocs);

    AjaxResult batchDel(List<Long> ids);

    PageList<CourseDoc> queryPageData(CourseDocQuery query);
}
