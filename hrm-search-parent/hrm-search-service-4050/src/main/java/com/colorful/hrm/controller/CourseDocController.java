package com.colorful.hrm.controller;

import com.colorful.hrm.doc.CourseDoc;
import com.colorful.hrm.query.CourseDocQuery;
import com.colorful.hrm.service.ICourseDocService;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/courseDoc")
public class CourseDocController {

    @Autowired
    private ICourseDocService courseDocService;
    //批量添加
    @PostMapping("/add")
    @PreAuthorize("hasAuthority('courseDoc:batchAdd')")
    public AjaxResult batchAdd(@RequestBody List<CourseDoc> courseDocs){
        return courseDocService.batchAdd(courseDocs);
    }

    //批量删除
    @PostMapping("/del")
    @PreAuthorize("hasAuthority('courseDoc:batchDel')")
    public AjaxResult batchDel(@RequestBody List<Long> ids){
        return courseDocService.batchDel(ids);
    }

    @PostMapping("/queryCourses")
    @PreAuthorize("isAnonymous()") //匿名访问
    public PageList<CourseDoc> queryPageData(@RequestBody CourseDocQuery query){
        return  courseDocService.queryPageData(query);
    }
}
