package com.colorful.hrm.doc;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * 针对于  Employee 表的文档映射
 * indexName：索引库
 * type：类型(表类型)
 */
@Document(indexName = "hrm" , type = "employee")
public class EmployeeDoc {
    @Id
    private Long id;
    @Field(type = FieldType.Keyword)//不分词文本
    private String name;

    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String text;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}