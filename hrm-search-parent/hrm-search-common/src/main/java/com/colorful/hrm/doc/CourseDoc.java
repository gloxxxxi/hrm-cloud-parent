package com.colorful.hrm.doc;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

@Document(indexName = "hrm" , type = "course")
//课程 name，forUser gradeName tenantName ...
//课程类型  courseTypeId courseTypeName
//影响信息  price priceOld charge
public class CourseDoc {
    //PUT  /hrmtest/coursetest/1
    @Id //文档对象的ID就是该字段的值
    private Long id;
    //课程名称
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String name;
    //价格
    @Field(type = FieldType.Float)
    private Float price;
    //原价
    @Field(type = FieldType.Float)
    private Float priceOld;
    //适用人群
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String forUser;
    //课程类型
    @Field(type = FieldType.Long)
    private Long courseTypeId;
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String courseTypeName;
    //等级名
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String gradeName;
    //机构
    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String tenantName;
    @Field(type = FieldType.Long)
    private Long tenantId;
    //图片
    @Field(type = FieldType.Keyword,index = false)
    private String pic;
    //销量
    @Field(type = FieldType.Integer)
    private Integer saleCount = 0;
    //浏览量
    @Field(type = FieldType.Integer)
    private Integer viewCount = 0;
    //评论数
    @Field(type = FieldType.Integer)
    private Integer commentCount = 0;
    //是否收费
    @Field(type = FieldType.Integer)
    private Integer charge;
    //上线时间
    @Field(type = FieldType.Date)
    private Date onlineTime;

//    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
//    private String all; //所有关键字要作用字段 假的，仅仅是用来搜索用的. 如果不这样做要做很多组合查询

//    public String getAll() {
//        return name+" "+forUser+" "+gradeName+" "+tenantName+" "+courseTypeName;
//    }
//
//    public void setAll(String all) {
//        this.all = all;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(Float priceOld) {
        this.priceOld = priceOld;
    }

    public String getForUser() {
        return forUser;
    }

    public void setForUser(String forUser) {
        this.forUser = forUser;
    }

    public Long getCourseTypeId() {
        return courseTypeId;
    }

    public void setCourseTypeId(Long courseTypeId) {
        this.courseTypeId = courseTypeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Integer getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(Integer saleCount) {
        this.saleCount = saleCount;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Integer getCharge() {
        return charge;
    }

    public void setCharge(Integer charge) {
        this.charge = charge;
    }

    public Date getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    public String getCourseTypeName() {
        return courseTypeName;
    }

    public void setCourseTypeName(String courseTypeName) {
        this.courseTypeName = courseTypeName;
    }
}