package com.colorful.hrm.query;

/**
 * @author 11695
 */
public class CourseDocQuery extends BaseQuery {
    private Long courseTypeId;
    private Long tenantId;
    private Integer priceMin;
    private Integer priceMax;
    private String sortField;
    private String sortType;

    public Long getCourseTypeId() {
        return courseTypeId;
    }

    public void setCourseTypeId(Long courseTypeId) {
        this.courseTypeId = courseTypeId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Integer priceMin) {
        this.priceMin = priceMin;
    }

    public Integer getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(Integer priceMax) {
        this.priceMax = priceMax;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    @Override
    public String toString() {
        return "CourseDocQuery{" +
                "courseTypeId=" + courseTypeId +
                ", tenantId='" + tenantId + '\'' +
                ", priceMin=" + priceMin +
                ", priceMax=" + priceMax +
                ", sortField='" + sortField + '\'' +
                ", sortType='" + sortType + '\'' +
                '}';
    }
}
