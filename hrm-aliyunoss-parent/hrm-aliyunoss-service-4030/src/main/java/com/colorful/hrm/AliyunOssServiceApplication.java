package com.colorful.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author SuperJellyfish
 * @description 阿里云oss对象存储服务启动类
 * @date 2022/1/3 18:03
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AliyunOssServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(AliyunOssServiceApplication.class, args);
    }
}
