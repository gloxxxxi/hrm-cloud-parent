package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.VipBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员登录账号 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface VipBaseMapper extends BaseMapper<VipBase> {

}
