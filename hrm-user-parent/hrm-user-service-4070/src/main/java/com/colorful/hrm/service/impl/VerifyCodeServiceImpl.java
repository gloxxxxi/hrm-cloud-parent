package com.colorful.hrm.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.colorful.hrm.client.ThirdPartyServiceClient;
import com.colorful.hrm.entity.VipBase;
import com.colorful.hrm.exception.ValidUtil;
import com.colorful.hrm.mapper.VipBaseMapper;
import com.colorful.hrm.service.IVerifyCodeService;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.UserConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VerifyCodeServiceImpl implements IVerifyCodeService {

    @Autowired
    private VipBaseMapper vipBaseMapper;

    @Autowired
    private ThirdPartyServiceClient thirdPartyServiceClient;
    @Override
    public AjaxResult sendRegisterSmsVerifyCode(Map<String, String> params) {
        String mobile = params.get("mobile");
        String imageCode = params.get("imageCode");
        String imageCodeKey = params.get("imageCodeKey");
        //1.验证 = 空值验证 + 是否注册过的验证
        ValidUtil.assertNotNull(mobile,"请输入正常手机号！");
        ValidUtil.assertNotNull(imageCodeKey,"请输入图片验证码");
        ValidUtil.assertNotNull(imageCode,"请输入图片验证码！");
        // 1.1 校验图片验证码是否ok 远程请求
        AjaxResult ajaxResult = thirdPartyServiceClient.verifyImgCode(params);
        ValidUtil.assertTrue(ajaxResult.isSuccess(),ajaxResult.getMessage());

        List<VipBase> users = vipBaseMapper.selectList(new EntityWrapper<VipBase>().eq("phone",mobile));
        ValidUtil.assertListIsNull(users,"该号码已经注册，请直接登录!!!");
        //获取redis中保存的验证码
        Map<String,String> paramsTmp = new HashMap<>();
        paramsTmp.put("key",UserConstants.REGISTER_CODE_PREFIX + mobile);
        paramsTmp.put("codeNum","4");
        paramsTmp.put("expire","3");
        paramsTmp.put("phone",mobile);
        return thirdPartyServiceClient.sendSmsVerifyCode(paramsTmp);
    }
}
