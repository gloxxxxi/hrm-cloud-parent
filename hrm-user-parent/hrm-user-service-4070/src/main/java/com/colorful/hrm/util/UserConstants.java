package com.colorful.hrm.util;


/**
 * 常量类
 */
public class UserConstants {

    /**
     * 注册时验证码的业务前缀
     */
    public static final String REGISTER_CODE_PREFIX = "register:";

    /**
     * 登录时验证码的业务前缀
     */
    public static final String LOGIN_CODE_PREFIX = "login:";

}
