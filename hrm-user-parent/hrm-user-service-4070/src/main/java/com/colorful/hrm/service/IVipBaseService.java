package com.colorful.hrm.service;

import com.colorful.hrm.dto.RegisterDto;
import com.colorful.hrm.entity.VipBase;
import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.util.AjaxResult;

/**
 * <p>
 * 会员登录账号 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface IVipBaseService extends IService<VipBase> {

    AjaxResult register(RegisterDto registerDto);
}
