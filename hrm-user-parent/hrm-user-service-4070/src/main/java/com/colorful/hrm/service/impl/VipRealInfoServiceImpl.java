package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.VipRealInfo;
import com.colorful.hrm.mapper.VipRealInfoMapper;
import com.colorful.hrm.service.IVipRealInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员实名资料 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
@Service
public class VipRealInfoServiceImpl extends ServiceImpl<VipRealInfoMapper, VipRealInfo> implements IVipRealInfoService {

}
