package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.VipUser;
import com.colorful.hrm.mapper.VipUserMapper;
import com.colorful.hrm.service.IVipUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员基本信息 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
@Service
public class VipUserServiceImpl extends ServiceImpl<VipUserMapper, VipUser> implements IVipUserService {

}
