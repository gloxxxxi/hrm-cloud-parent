package com.colorful.hrm.service;

import com.colorful.hrm.entity.VipAddress;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 收货地址 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface IVipAddressService extends IService<VipAddress> {

}
