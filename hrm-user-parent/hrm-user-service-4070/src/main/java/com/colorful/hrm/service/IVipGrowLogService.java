package com.colorful.hrm.service;

import com.colorful.hrm.entity.VipGrowLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 成长值记录 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface IVipGrowLogService extends IService<VipGrowLog> {

}
