package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.VipRealInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员实名资料 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface VipRealInfoMapper extends BaseMapper<VipRealInfo> {

}
