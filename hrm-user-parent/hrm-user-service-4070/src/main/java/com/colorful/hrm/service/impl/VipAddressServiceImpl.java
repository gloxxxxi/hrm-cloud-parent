package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.VipAddress;
import com.colorful.hrm.mapper.VipAddressMapper;
import com.colorful.hrm.service.IVipAddressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收货地址 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
@Service
public class VipAddressServiceImpl extends ServiceImpl<VipAddressMapper, VipAddress> implements IVipAddressService {

}
