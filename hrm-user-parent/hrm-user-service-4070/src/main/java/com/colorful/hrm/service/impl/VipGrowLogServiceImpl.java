package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.VipGrowLog;
import com.colorful.hrm.mapper.VipGrowLogMapper;
import com.colorful.hrm.service.IVipGrowLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 成长值记录 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
@Service
public class VipGrowLogServiceImpl extends ServiceImpl<VipGrowLogMapper, VipGrowLog> implements IVipGrowLogService {

}
