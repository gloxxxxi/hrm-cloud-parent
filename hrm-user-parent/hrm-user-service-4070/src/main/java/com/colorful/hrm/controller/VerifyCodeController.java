package com.colorful.hrm.controller;

import com.colorful.hrm.service.IVerifyCodeService;
import com.colorful.hrm.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/verifycode")
public class VerifyCodeController {

    @Autowired
    private IVerifyCodeService verifyCodeService;

    @PostMapping("/registerSmsCode")
    public AjaxResult sendRegisterSmsCode(@RequestBody Map<String,String> params){
        return verifyCodeService.sendRegisterSmsVerifyCode(params);
    }
}
