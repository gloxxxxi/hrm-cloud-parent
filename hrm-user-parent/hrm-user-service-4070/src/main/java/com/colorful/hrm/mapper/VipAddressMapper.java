package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.VipAddress;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 收货地址 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface VipAddressMapper extends BaseMapper<VipAddress> {

}
