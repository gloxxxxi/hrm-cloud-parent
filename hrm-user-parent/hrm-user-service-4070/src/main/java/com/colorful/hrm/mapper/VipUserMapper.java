package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.VipUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员基本信息 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface VipUserMapper extends BaseMapper<VipUser> {

}
