package com.colorful.hrm.service;

import com.colorful.hrm.util.AjaxResult;

import java.util.Map;

public interface IVerifyCodeService {
    AjaxResult sendRegisterSmsVerifyCode(Map<String, String> params);
}
