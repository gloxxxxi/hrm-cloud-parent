package com.colorful.hrm.service;

import com.colorful.hrm.entity.VipRealInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员实名资料 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface IVipRealInfoService extends IService<VipRealInfo> {

}
