package com.colorful.hrm.service;

import com.colorful.hrm.entity.VipUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员基本信息 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface IVipUserService extends IService<VipUser> {

}
