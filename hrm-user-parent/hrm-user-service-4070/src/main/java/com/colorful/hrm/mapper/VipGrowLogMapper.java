package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.VipGrowLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 成长值记录 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
public interface VipGrowLogMapper extends BaseMapper<VipGrowLog> {

}
