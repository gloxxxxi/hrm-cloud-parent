package com.colorful.hrm.service.impl;

import com.colorful.hrm.client.LoginUserClient;
import com.colorful.hrm.client.ThirdPartyServiceClient;
import com.colorful.hrm.domain.LoginUser;
import com.colorful.hrm.dto.RegisterDto;
import com.colorful.hrm.entity.VipBase;
import com.colorful.hrm.entity.VipUser;
import com.colorful.hrm.exception.ValidUtil;
import com.colorful.hrm.mapper.VipBaseMapper;
import com.colorful.hrm.mapper.VipUserMapper;
import com.colorful.hrm.service.IVipBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.BitStatesUtils;
import com.colorful.hrm.util.UserConstants;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 会员登录账号 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-13
 */
@Service
public class VipBaseServiceImpl extends ServiceImpl<VipBaseMapper, VipBase> implements IVipBaseService {
    @Autowired
    private ThirdPartyServiceClient thirdPartyServiceClient;
    @Autowired
    private LoginUserClient loginUserClient;
    @Autowired
    private VipBaseMapper vipBaseMapper;

    @Autowired
    private VipUserMapper vipUserMapper;
    @Override
//    @Transactional
    @GlobalTransactional
    public AjaxResult register(RegisterDto registerDto) {
        //1校验 null+短信验证码+用户检验
        Map<String,String> params = new HashMap<>();
        params.put("key", UserConstants.REGISTER_CODE_PREFIX+registerDto.getMobile());
        params.put("code",registerDto.getSmsCode());
        AjaxResult result = thirdPartyServiceClient.verifySmsCode(params);
        ValidUtil.assertTrue(result.isSuccess(),result.getMessage());
        //2 远程调用hrm-auth保存loginUser并获取到loginId
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername(registerDto.getMobile());
//        loginUser.setPassword(registerDto.getPassword());
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String enPass = bCryptPasswordEncoder.encode(registerDto.getPassword());
        loginUser.setPassword(enPass);
        loginUser.setType(1);
        AjaxResult ajaxResult = loginUserClient.addOrUpdate(loginUser);
        ValidUtil.assertTrue(ajaxResult.isSuccess(),ajaxResult.getMessage());
        Long loginId = Long.valueOf(ajaxResult.getResultObj().toString()) ;
        //3 保存vipBase，vipuser
        VipBase vipBase = new VipBase();
//        vipBase.setPassword(registerDto.getPassword());
        vipBase.setPassword(enPass);
        vipBase.setPhone(registerDto.getMobile());
        vipBase.setNickName(registerDto.getMobile());
        // 状态表示法
        // 互斥状态 启用，禁止， 上线，下线
        // 1. 使用boolean 只有true和false
        // 2. *使用int 一个数字表示一个状态有更多的拓展性
        // 并行状态 一个时刻同时存在多个状态 - 已经注册并手机实名认证并激活
        // 1. 一个状态一个字段，字段多，不好管理，拓展性差
        // 2. *二进制位表示状态，一个二进制位表示一种状态，多个就是多种状态
        long state = BitStatesUtils.OP_REGISTED;
        state = BitStatesUtils.addState(state, BitStatesUtils.OP_ACTIVED);
        state = BitStatesUtils.addState(state, BitStatesUtils.OP_AUTHED_PHONE);
        vipBase.setBitState(state);
        vipBase.setLoginId(loginId);
        vipBaseMapper.insert(vipBase);

        VipUser vipUser = new VipUser();
        vipUser.setVipId(vipBase.getId());
        vipUserMapper.insert(vipUser);
        return AjaxResult.me();
    }
}
