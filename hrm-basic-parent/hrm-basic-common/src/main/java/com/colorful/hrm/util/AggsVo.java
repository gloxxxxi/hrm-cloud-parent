package com.colorful.hrm.util;

public class AggsVo {
    private Long id; //tenantId courseTypeId ....
    private String name; //tenantName courseTypeName ...
    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "AggsVo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", count=" + count +
                '}';
    }

    public AggsVo(Long id, String name, Long count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public AggsVo() {
    }
}