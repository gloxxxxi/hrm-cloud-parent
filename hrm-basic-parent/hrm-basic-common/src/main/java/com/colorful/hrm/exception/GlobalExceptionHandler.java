package com.colorful.hrm.exception;

import com.colorful.hrm.util.AjaxResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * @author SuperJellyfish
 * @description 全局异常处理
 *              在controller前后执行逻辑。底层aop
 * @date 2022/1/3 10:51
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 捕获全局异常
     * @param e 异常
     * @return 异常结果
     */
    @ExceptionHandler(GlobalException.class)
    public AjaxResult globalExceptionHandler(GlobalException e) {

        return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
    }

    /**
     * 捕获jsr303参数校验异常
     * @param e 异常
     * @return 异常结果
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public AjaxResult exceptionHandle(MethodArgumentNotValidException e){

        //获取所有字段错误
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();

        //遍历所有字段错误进行拼接
        StringBuilder message = new StringBuilder();
        fieldErrors.forEach(fieldError -> {
            message.append(fieldError.getDefaultMessage()+",");
        });
        //去除最后一个逗号
        String str = message.toString();
        str = str.substring(0,str.length()-1);
        return AjaxResult.me().setSuccess(false)
                .setErrorCode(ErrorCode.CODE_400_PARAM_VALID.getCode())
                .setMessage(str);
    }

    /**
     * 捕获其他异常
     * @param e 异常
     * @return 异常结果
     */
    @ExceptionHandler(Exception.class)
    public AjaxResult exceptionHandler(GlobalException e) {
        return AjaxResult.me().setSuccess(false).setMessage("系统错误，正在殴打程序员");
    }
}
