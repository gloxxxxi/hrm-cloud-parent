package com.colorful.hrm.exception;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author SuperJellyfish
 * @description 有效判断
 * @date 2022/1/3 10:30
 */
public class ValidUtil {

    /** 手机号的验证规则 */
    private static final String PHONE_REGEX = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0-9]))\\d{8}$";


    /**  邮箱的校验规则 */
    private static final String EMAIL_REGEX = "[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?";



    /**
     * 断言验证手机号
     * @param phone
     * @param message
     */
    public static void assertPhone(String phone,String message){
        //格式判断
        Pattern p = Pattern.compile(PHONE_REGEX);
        Matcher m = p.matcher(phone);
        if(!m.matches()){
            throw new GlobalException(message);
        }
    }

    /**
     * 电话校验
     * @param phone
     */
    public static void assertPhone(String phone){
        assertPhone(phone,"无效的手机号");
    }


    /**
     *     //断言验证邮箱
     * @param email
     * @param message
     */
    public static void assertEmail(String email,String message){
        //格式判断
        Pattern p = Pattern.compile(EMAIL_REGEX);
        Matcher m = p.matcher(email);
        if(!m.matches()){
            throw new GlobalException(message);
        }
    }
    //断言true
    public static void assertNull(Object obj, String message){
        if (obj!=null) {
            throw new GlobalException(message);
        }
    }
    public static void assertEmail(String email){
        assertEmail(email,"无效的邮箱");
    }

    /**
     * 断言对象不为空，否则报错
     * @param o 对象
     * @param message 错误信息
     */
    public static void assertNotNull(Object o, String message) {
        if (null == o) {
            throw new GlobalException(message);
        }
        if (o instanceof String) {
            String str = (String) o;
            if (str.length() < 1) {
                throw new GlobalException(message);
            }
        }
    }
    public static void assertNotNull(Object obj,ErrorCode errorCode){

        //对象不为Null
        if (obj==null) {
            throw new GlobalException(errorCode.getCode(),errorCode.getMessage());
        }
        //string的""判断
        if (obj instanceof String){
            String objStr = (String) obj;
            if (objStr.length()<1) {
                throw  new GlobalException(errorCode.getCode(),errorCode.getMessage());
            }
        }
    }
    /**
     * 断言集合不为空
     * @param lists
     * @param message
     */
    public static void assertListNotNull(List<?> lists, String message) {
        if (null == lists || lists.size() < 1) {
            throw new GlobalException(message);
        }
    }

    /**
     * 断言集合为空
     * @param lists
     * @param message
     */
    public static void assertListIsNull(List<?> lists, String message) {
        if (null != lists && lists.size()>0) {
            throw new GlobalException(message);
        }
    }
    public static void assertListNotNull(List<?> lists,ErrorCode errorCode){
        if (lists==null || lists.size()==0) {
            throw new GlobalException(errorCode.getCode(),errorCode.getMessage());
        }
    }

    //断言为null,不为null就要报错
    public static void assertListIsNull(List<?> lists,ErrorCode errorCode){
        if (lists!=null && lists.size()>0) {
            throw new GlobalException(errorCode.getCode(),errorCode.getMessage());
        }
    }
    //断言true
    public static void assertTrue(Boolean b, String message){
        if (!b) {
            throw new GlobalException(message);
        }
    }

}
