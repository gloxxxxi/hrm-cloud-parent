package com.colorful.hrm.exception;

/**
 * @author SuperJellyfish
 * @description 全局异常
 * @date 2022/1/3 10:30
 */
public class GlobalException extends RuntimeException{
    /** 错误码 */
    private int errorCode = 500;
    /**
     *  //默认构造函数
     */
    public GlobalException() {
    }



    /**
     * //带错误消息构造
     * @param message 错误消息
     */
    public GlobalException(String message) {
        super(message);
    }

    /**
     *  //带错误消息+异常构造
     * @param message 错误消息
     * @param cause 异常构造
     */
    public GlobalException(String message, Throwable cause) {
        super(message, cause);
    }
    //支持错误码构造
    public GlobalException(int errorCode,String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
