package com.colorful.hrm.util;

//Ajax请求响应对象的类
public class AjaxResult {
    //错误码
    private int errorCode = 200;
    private boolean success = true;
    private String message = "操作成功!";


    //返回到前台对象
    private Object resultObj;

    public boolean isSuccess() {
        return success;
    }

    public AjaxResult setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public AjaxResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getResultObj() {
        return resultObj;
    }

    public AjaxResult setResultObj(Object resultObj) {
        this.resultObj = resultObj;
        return this;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public AjaxResult setErrorCode(int errorCode) {
        this.errorCode = errorCode;
        return this;
    }


    //AjaxResult.me()成功
    //AjaxResult.me().setMessage()成功
    //AjaxResult.me().setSuccess(false),setMessage("失败");
    public  static AjaxResult me(){
        return new AjaxResult();
    }
}
