package com.colorful.hrm.exception;

/**
 * 错误码枚举
 * @author 11695
 */
public enum ErrorCode {
    //不可为空的错误码
    CODE_400_PARAM_VALID(400001,"参数校验错误！"),
    CODE_400_COURSE_GRADE_ILLEGAL(400006,"输入课程等级非法！"),
    CODE_400_TENANT_EXSIT(400003,"租户已经入驻！直接登录！"),
    CODE_400_COURSE_LIST_ILLEGAL(400007,"请选择对应的课程后再操作！"),
    CODE_400_MEAL_NOT_NULL(400004,"请选择套餐！"),
    CODE_400_COURSE_TYPE_CRUMBS_ILLEGAL(400008,"面包屑非法！"),
    CODE_400_ADMIN_EXSIT(400005,"您已经入驻过店铺了！登录后可以添加新的店铺！"),
    CODE_500_SYSTEM_ERROR(500001,"内部错误，请联系管理员！"),
    CODE_400002_PWD_EQUAL(400002,"两次密码不一致！");

    private int code;
    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}