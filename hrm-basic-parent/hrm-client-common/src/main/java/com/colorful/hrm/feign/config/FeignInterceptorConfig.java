package com.colorful.hrm.feign.config;

import com.colorful.hrm.feign.interceptor.DefaultOAuth2FeignRequestInterceptor;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignInterceptorConfig {
    @Bean
    public RequestInterceptor oAuth2RequestInterceptor() {
        return new DefaultOAuth2FeignRequestInterceptor();
    }
}