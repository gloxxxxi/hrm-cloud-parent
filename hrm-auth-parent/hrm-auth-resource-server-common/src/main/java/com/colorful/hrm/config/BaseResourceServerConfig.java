package com.colorful.hrm.config;

import com.colorful.hrm.constants.HrmResourceConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @author 11695
 */
////资源服务配置
//@Configuration
////开启资源服务配置
//@EnableResourceServer
////开启方法授权
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class BaseResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Value("${oauth2.resourceId}")
    private String resourceId;

    //修改JWT令牌
    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    //JWT令牌校验工具
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        //设置JWT签名密钥。它可以是简单的MAC密钥，也可以是RSA密钥
        jwtAccessTokenConverter.setSigningKey(HrmResourceConstants.JWT_SIGNING_KEY);
        return jwtAccessTokenConverter;
    }
    //1.资源服务安全配置
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        //我的资源名称是什么
        resources.resourceId(resourceId);
        //用来校验，解析Token的服务
        //resources.tokenServices(resourceServerTokenServices()); //非jwttoken方式
        resources.tokenStore(tokenStore());

    }

}