package com.colorful.hrm.handler;

import com.alibaba.fastjson.JSON;
import com.colorful.hrm.vo.UserContextInfo;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

//UserContextInfo的持有者
public class UserContextInfoHolder {

    /**
     * 获取UserContextInfo
     * @return
     */
    public static UserContextInfo getUserContextInfo(){
        SecurityContext context = SecurityContextHolder.getContext();
        String principalStr = (String) context.getAuthentication().getPrincipal();
        UserContextInfo userContextInfo = JSON.parseObject(principalStr, UserContextInfo.class);
        return userContextInfo;
    }
}