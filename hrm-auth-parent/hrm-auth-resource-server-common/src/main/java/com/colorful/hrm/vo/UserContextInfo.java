package com.colorful.hrm.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 11695
 */
public class UserContextInfo {
    private Long loginUserId;
    private String loginUsername;
    private Long userId;
    private String username;
    private Long employeeId;
    private String empUsername;
    private Long tenantId;
    private String tenantName;
    private List<String> permissions = new ArrayList<>();

    public Long getLoginUserId() {
        return loginUserId;
    }

    public void setLoginUserId(Long loginUserId) {
        this.loginUserId = loginUserId;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmpUsername() {
        return empUsername;
    }

    public void setEmpUsername(String empUsername) {
        this.empUsername = empUsername;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "UserContextInfo{" +
                "loginUserId=" + loginUserId +
                ", loginUsername='" + loginUsername + '\'' +
                ", userId=" + userId +
                ", username='" + username + '\'' +
                ", employeeId=" + employeeId +
                ", empUsername='" + empUsername + '\'' +
                ", tenantId=" + tenantId +
                ", tenantName='" + tenantName + '\'' +
                '}';
    }
}