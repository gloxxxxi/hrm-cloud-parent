package com.colorful.hrm.client;

import com.colorful.hrm.domain.LoginUser;
import com.colorful.hrm.util.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 11695
 */
@FeignClient(value = "AUTH-SERVICE",fallbackFactory =LoginUserClientFallbackFactory.class )
@RequestMapping("/loginUser")
public interface LoginUserClient {
    @PutMapping
    AjaxResult addOrUpdate(@RequestBody LoginUser loginUser);
}


