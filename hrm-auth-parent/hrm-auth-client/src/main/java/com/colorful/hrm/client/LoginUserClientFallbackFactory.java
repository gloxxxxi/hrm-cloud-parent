package com.colorful.hrm.client;

import com.colorful.hrm.domain.LoginUser;
import com.colorful.hrm.util.AjaxResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class LoginUserClientFallbackFactory implements FallbackFactory<LoginUserClient> {
    @Override
    public LoginUserClient create(Throwable throwable) {
        return new LoginUserClient() {
            @Override
            public AjaxResult addOrUpdate(LoginUser loginUser) {
                return AjaxResult.me().setSuccess(false).setMessage("系统维护中.....");
            }
        };
    }
}
