package com.colorful.hrm.constants;

public class LoginConstants {
    //前台用户
    public static final int  LOGIN_TYPE_USER =  1;
    //后台管理员
    public static final int  LOGIN_TYPE_ADMIN =  0;

    //密码授权地址
    public static  final  String PASSWORD_AUTH_URL =
            "http://localhost:3000/services/auth/oauth/token?client_id=%s&client_secret=%s&grant_type=password&username=%s&password=%s";
}