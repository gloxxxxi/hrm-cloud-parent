package com.colorful.hrm.mapper;

import com.colorful.hrm.domain.LoginUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
public interface LoginUserMapper extends BaseMapper<LoginUser> {

}
