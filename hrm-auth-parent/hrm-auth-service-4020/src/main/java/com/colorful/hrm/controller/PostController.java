package com.colorful.hrm.controller;

import com.colorful.hrm.service.IPostService;
import com.colorful.hrm.domain.Post;
import com.colorful.hrm.query.PostQuery;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/post")
public class PostController {
    @Autowired
    public IPostService postService;


    /**
     * 保存和修改公用的
     * @param post  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Post post){
        try {
            if( post.getId()!=null)
                postService.updateById(post);
            else
                postService.insert(post);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            postService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public Post get(@PathVariable("id")Long id)
    {
        return postService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public List<Post> list(){

        return postService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<Post> json(@RequestBody PostQuery query)
    {
        Page<Post> page = new Page<Post>(query.getPage(),query.getRows());
        page = postService.selectPage(page);
        return new PageList<Post>(page.getTotal(),page.getRecords());
    }
}
