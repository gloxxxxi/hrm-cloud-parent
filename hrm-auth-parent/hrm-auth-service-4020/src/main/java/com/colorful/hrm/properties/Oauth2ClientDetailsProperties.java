package com.colorful.hrm.properties;

import com.colorful.hrm.constants.LoginConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "oauth2.client")
public class Oauth2ClientDetailsProperties {
    private Oauth2ClientDetails admin;
    private Oauth2ClientDetails website;

    public Oauth2ClientDetails getAdmin() {
        return admin;
    }

    public void setAdmin(Oauth2ClientDetails admin) {
        this.admin = admin;
    }

    public Oauth2ClientDetails getWebsite() {
        return website;
    }

    public void setWebsite(Oauth2ClientDetails website) {
        this.website = website;
    }

    //通过用户类型，返回admin或者website
    public Oauth2ClientDetails getClientDetials(Integer type) {
        switch (type.intValue()){
            case LoginConstants.LOGIN_TYPE_ADMIN:
                return admin;
            case LoginConstants.LOGIN_TYPE_USER:
                return website;
            default:
                return admin;
        }
    }
}