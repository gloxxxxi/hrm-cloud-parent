package com.colorful.hrm.userdetails;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.colorful.hrm.client.SysmanageServerClient;
import com.colorful.hrm.constants.LoginConstants;
import com.colorful.hrm.domain.LoginUser;
import com.colorful.hrm.domain.Permission;
import com.colorful.hrm.mapper.LoginUserMapper;
import com.colorful.hrm.mapper.PermissionMapper;
import com.colorful.hrm.vo.UserContextInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class HRMUserDetailsService implements UserDetailsService {
    @Autowired
    private LoginUserMapper loginUserMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private SysmanageServerClient sysmanageServerClient;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Wrapper<LoginUser> wrapper = new EntityWrapper<LoginUser>()
                .eq("username", username)
                .or()
                .eq("email", username)
                .or()
                .eq("phone", username);
        List<LoginUser> loginUsers = loginUserMapper.selectList(wrapper);
        if (loginUsers==null || loginUsers.size()<1)
            return null;

        LoginUser loginUser = loginUsers.get(0);
        //后台管理任意才去查询权限
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if (loginUser.getType().intValue()== LoginConstants.LOGIN_TYPE_ADMIN){
            //1 从数据库中查询权限
            List<Permission> permissions = permissionMapper
                    .loadPermissionsByUserId(loginUser.getId());

            //2 转换为SPringsecurity所需要权限数据
            authorities = permissions.stream().map(permission -> {
                return new SimpleGrantedAuthority(permission.getSn());
            }).collect(Collectors.toList());

            // 通过sysmanage获取上下文，坑：没有开启sysmanage-service的服务的//开启资源服务配置，解析不了token，userContextInfo会nukk，
            // 因为直接回到了fallback处理为null
            UserContextInfo userContextInfo = sysmanageServerClient
                    .getEmpAndTenantByLoginId(loginUser.getId());
            userContextInfo.setLoginUserId(loginUser.getId());
            userContextInfo.setLoginUsername(loginUser.getUsername());
            System.out.println(userContextInfo);
            return new User(JSON.toJSONString(userContextInfo),loginUser.getPassword(),authorities);
        }else {
            UserContextInfo userContextInfo = new UserContextInfo();
            userContextInfo.setLoginUserId(loginUser.getId());
            userContextInfo.setLoginUsername(loginUser.getUsername());
            //userId username和上边一样，要远程发起请求去调用。 @TODO
            return new User(JSON.toJSONString(userContextInfo),loginUser.getPassword(),authorities);
        }

    }
}
