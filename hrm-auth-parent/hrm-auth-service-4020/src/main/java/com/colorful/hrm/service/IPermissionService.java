package com.colorful.hrm.service;

import com.colorful.hrm.domain.Permission;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
public interface IPermissionService extends IService<Permission> {

}
