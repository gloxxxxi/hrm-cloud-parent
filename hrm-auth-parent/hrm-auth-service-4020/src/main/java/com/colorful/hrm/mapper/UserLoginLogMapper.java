package com.colorful.hrm.mapper;

import com.colorful.hrm.domain.UserLoginLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 登录记录 Mapper 接口
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
public interface UserLoginLogMapper extends BaseMapper<UserLoginLog> {

}
