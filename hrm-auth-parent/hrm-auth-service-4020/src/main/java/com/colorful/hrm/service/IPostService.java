package com.colorful.hrm.service;

import com.colorful.hrm.domain.Post;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 岗位信息表 服务类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
public interface IPostService extends IService<Post> {

}
