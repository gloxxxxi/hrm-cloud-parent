package com.colorful.hrm.service;

import com.colorful.hrm.domain.LoginUser;
import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.dto.LoginDto;
import com.colorful.hrm.util.AjaxResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
public interface ILoginUserService extends IService<LoginUser> {

    AjaxResult loginAccount(LoginDto loginDto);
}
