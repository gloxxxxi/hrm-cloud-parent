package com.colorful.hrm.mapper;

import com.colorful.hrm.domain.Menu;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
