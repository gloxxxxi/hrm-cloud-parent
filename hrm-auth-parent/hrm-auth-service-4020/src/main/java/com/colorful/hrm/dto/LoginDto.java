package com.colorful.hrm.dto;

import javax.validation.constraints.NotEmpty;

public class LoginDto {
    @NotEmpty(message = "请输入用户名！")
    private String username;
    @NotEmpty(message = "请输入密码！")
    private String password;
    @NotEmpty(message = "参数非法！")
    private Integer type;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "LoginDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", type=" + type +
                '}';
    }
}