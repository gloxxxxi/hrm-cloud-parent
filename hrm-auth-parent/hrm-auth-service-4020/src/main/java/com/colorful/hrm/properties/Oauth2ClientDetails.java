package com.colorful.hrm.properties;

public class Oauth2ClientDetails {
    private String clientId;
    private String secret;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        return "Oauth2ClientDetails{" +
                "clientId='" + clientId + '\'' +
                ", secret='" + secret + '\'' +
                '}';
    }
}