package com.colorful.hrm.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.colorful.hrm.constants.LoginConstants;
import com.colorful.hrm.domain.LoginUser;
import com.colorful.hrm.dto.LoginDto;
import com.colorful.hrm.exception.ValidUtil;
import com.colorful.hrm.mapper.LoginUserMapper;
import com.colorful.hrm.properties.Oauth2ClientDetails;
import com.colorful.hrm.properties.Oauth2ClientDetailsProperties;
import com.colorful.hrm.service.ILoginUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
@Service
public class LoginUserServiceImpl extends ServiceImpl<LoginUserMapper, LoginUser> implements ILoginUserService {

    @Autowired
    private LoginUserMapper loginUserMapper;

    @Autowired
    private Oauth2ClientDetailsProperties properties;

    @Override
    public AjaxResult loginAccount(LoginDto loginDto) {
        //1 参数校验：null，用户是否存在
        String username = loginDto.getUsername(); //作用于三个字段username,emial,phone


        Wrapper<LoginUser> wrapper = new EntityWrapper<LoginUser>()
                .eq("username", username)
                .or()
                .eq("email", username)
                .or()
                .eq("phone", username);
        List<LoginUser> loginUsers = loginUserMapper.selectList(wrapper);
        Integer type = loginUsers.get(0).getType();
//        Integer type = loginDto.getType();
        ValidUtil.assertListNotNull(loginUsers,"请输入正确用户名或密码！");
        //2 发起请求获取token-httpclient
        //http://localhost:4020/oauth/token?client_id=admin&client_secret=1&grant_type=password&username=18244444444&password=123456
        //zuul地址：http://localhost:3010/services/auth/oauth/token?client_id=admin&client_secret=1&grant_type=password&username=18244444444&password=123456
        // 方案1：写死clientid和写死clientid和client
        /*
       String clientId = "website";
       String clientSecret="1";
        if (type.longValue()==0){
            clientId="admin";
            clientSecret="1";
        }*/
        //方案2：可配置
         Oauth2ClientDetails clientDetials = properties.getClientDetials(type);
        ValidUtil.assertNotNull(clientDetials,"非法参数");

        String pwdAuthUrl = String.format(LoginConstants.PASSWORD_AUTH_URL,
                clientDetials.getClientId(), clientDetials.getSecret(),
                loginDto.getUsername(), loginDto.getPassword());
        //发送请求获取数据
        Map<String, Object> map = HttpUtil.sendPost(pwdAuthUrl);
        //错误判断
        String error = (String) map.get("error");
        String message = (String) map.get("error_description");
        ValidUtil.assertNull(error,message);


        //成功后获取token和refreshToken
        String token = (String) map.get("access_token");
        String refreshToken = (String) map.get("refresh_token");

        //构造返回值
        HashMap<String, String> result = new HashMap<>();
        result.put("token",token);
        result.put("refreshToken",refreshToken);
        return AjaxResult.me().setResultObj(result);
    }
}
