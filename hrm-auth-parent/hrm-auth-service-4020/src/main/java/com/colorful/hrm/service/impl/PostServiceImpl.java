package com.colorful.hrm.service.impl;

import com.colorful.hrm.domain.Post;
import com.colorful.hrm.mapper.PostMapper;
import com.colorful.hrm.service.IPostService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 岗位信息表 服务实现类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
@Service
public class PostServiceImpl extends ServiceImpl<PostMapper, Post> implements IPostService {

}
