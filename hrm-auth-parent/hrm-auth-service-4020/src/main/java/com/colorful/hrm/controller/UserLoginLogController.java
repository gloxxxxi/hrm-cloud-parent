package com.colorful.hrm.controller;

import com.colorful.hrm.service.IUserLoginLogService;
import com.colorful.hrm.domain.UserLoginLog;
import com.colorful.hrm.query.UserLoginLogQuery;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/userLoginLog")
public class UserLoginLogController {
    @Autowired
    public IUserLoginLogService userLoginLogService;


    /**
     * 保存和修改公用的
     * @param userLoginLog  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody UserLoginLog userLoginLog){
        try {
            if( userLoginLog.getId()!=null) {
                userLoginLogService.updateById(userLoginLog);
            } else {
                userLoginLogService.insert(userLoginLog);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            userLoginLogService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public UserLoginLog get(@PathVariable("id")Long id)
    {
        return userLoginLogService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public List<UserLoginLog> list(){

        return userLoginLogService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<UserLoginLog> json(@RequestBody UserLoginLogQuery query)
    {
        Page<UserLoginLog> page = new Page<UserLoginLog>(query.getPage(),query.getRows());
        page = userLoginLogService.selectPage(page);
        return new PageList<UserLoginLog>(page.getTotal(),page.getRecords());
    }
}
