package com.colorful.hrm.service.impl;

import com.colorful.hrm.domain.Menu;
import com.colorful.hrm.mapper.MenuMapper;
import com.colorful.hrm.service.IMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

}
