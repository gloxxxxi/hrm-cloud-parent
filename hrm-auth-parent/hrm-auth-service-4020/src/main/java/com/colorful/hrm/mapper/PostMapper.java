package com.colorful.hrm.mapper;

import com.colorful.hrm.domain.Post;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 岗位信息表 Mapper 接口
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
public interface PostMapper extends BaseMapper<Post> {

}
