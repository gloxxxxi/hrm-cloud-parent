package com.colorful.hrm.service;

import com.colorful.hrm.domain.UserLoginLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 登录记录 服务类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
public interface IUserLoginLogService extends IService<UserLoginLog> {

}
