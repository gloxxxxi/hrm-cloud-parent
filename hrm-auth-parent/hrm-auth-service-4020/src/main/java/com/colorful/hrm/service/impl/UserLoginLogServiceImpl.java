package com.colorful.hrm.service.impl;

import com.colorful.hrm.domain.UserLoginLog;
import com.colorful.hrm.mapper.UserLoginLogMapper;
import com.colorful.hrm.service.IUserLoginLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录记录 服务实现类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
@Service
public class UserLoginLogServiceImpl extends ServiceImpl<UserLoginLogMapper, UserLoginLog> implements IUserLoginLogService {

}
