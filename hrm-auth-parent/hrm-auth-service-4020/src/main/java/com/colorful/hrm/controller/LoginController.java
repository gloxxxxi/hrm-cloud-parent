package com.colorful.hrm.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @RequestMapping("/loginSuccess")
    public String loginSuccess(){
        return "登錄成功";
    }
}