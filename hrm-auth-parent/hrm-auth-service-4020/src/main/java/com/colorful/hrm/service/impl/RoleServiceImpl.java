package com.colorful.hrm.service.impl;

import com.colorful.hrm.domain.Role;
import com.colorful.hrm.mapper.RoleMapper;
import com.colorful.hrm.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
