package com.colorful.hrm.service.impl;

import com.colorful.hrm.domain.Permission;
import com.colorful.hrm.mapper.PermissionMapper;
import com.colorful.hrm.service.IPermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author SuperJellyfish
 * @since 2022-01-02
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

}
