package com.colorful.hrm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * @author 11695
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.colorful.hrm.mapper")
//@EnableResourceServer
@EnableFeignClients
public class AuthServerApp {
    public static void main(String[] args) {
        SpringApplication.run(AuthServerApp.class,args);
    }
}
