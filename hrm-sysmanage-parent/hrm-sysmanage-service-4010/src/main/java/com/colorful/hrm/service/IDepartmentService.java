package com.colorful.hrm.service;

import com.colorful.hrm.entity.Department;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface IDepartmentService extends IService<Department> {

}
