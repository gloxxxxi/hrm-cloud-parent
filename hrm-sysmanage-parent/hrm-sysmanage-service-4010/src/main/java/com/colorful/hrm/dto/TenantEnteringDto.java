package com.colorful.hrm.dto;

import com.colorful.hrm.entity.Employee;
import com.colorful.hrm.entity.Tenant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author 11695
 */
public class TenantEnteringDto {
    private Employee employee;

    private Tenant tenant;
    @NotNull(message = "请选择至少一个套餐")
    private Long mealId;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public Long getMealId() {
        return mealId;
    }

    public void setMealId(Long mealId) {
        this.mealId = mealId;
    }
}