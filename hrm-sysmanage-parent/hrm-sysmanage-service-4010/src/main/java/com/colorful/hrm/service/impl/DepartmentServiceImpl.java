package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.Department;
import com.colorful.hrm.mapper.DepartmentMapper;
import com.colorful.hrm.service.IDepartmentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

}
