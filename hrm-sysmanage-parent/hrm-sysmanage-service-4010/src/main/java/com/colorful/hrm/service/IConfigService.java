package com.colorful.hrm.service;

import com.colorful.hrm.entity.Config;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface IConfigService extends IService<Config> {

}
