package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.Config;
import com.colorful.hrm.mapper.ConfigMapper;
import com.colorful.hrm.service.IConfigService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements IConfigService {

}
