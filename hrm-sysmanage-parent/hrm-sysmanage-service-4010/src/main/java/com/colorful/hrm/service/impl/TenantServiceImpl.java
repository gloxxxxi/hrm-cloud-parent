package com.colorful.hrm.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.colorful.hrm.client.LoginUserClient;
import com.colorful.hrm.domain.LoginUser;
import com.colorful.hrm.dto.TenantEnteringDto;
import com.colorful.hrm.entity.Employee;
import com.colorful.hrm.entity.Tenant;
import com.colorful.hrm.exception.ValidUtil;
import com.colorful.hrm.mapper.EmployeeMapper;
import com.colorful.hrm.mapper.TenantMapper;
import com.colorful.hrm.query.TenantQuery;
import com.colorful.hrm.service.ITenantService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
@Service
public class TenantServiceImpl extends ServiceImpl<TenantMapper, Tenant> implements ITenantService {
    @Resource
    private TenantMapper tenantMapper;
    @Resource
    private EmployeeMapper employeeMapper;
    @Autowired
    private LoginUserClient loginUserClient;
    @Override
    public PageList<Tenant> selectPageList(TenantQuery query) {

        //封装page对象
        Page<Tenant> page = new Page<>(query.getPage(),query.getRows());
        //查询当前页数据
        List<Tenant> datas = tenantMapper.loadPageList(page,query);
        return new PageList<>(page.getTotal(),datas);
    }

    @Override
    public void entering(TenantEnteringDto enteringDto) {
        // 1 参数校验
        //1.1 null检验
        // 做了jsr303验证不需要断言验证
//        ValidUtil.assertNotNull(enteringDto.getMealId(), "请选择套餐");
        //1.2 判断租户是否已经存在
        EntityWrapper<Tenant> wrapper = new EntityWrapper<>();
        Tenant tenant = enteringDto.getTenant();
        wrapper.eq("company_name", tenant.getCompanyName());
        List<Tenant> list = tenantMapper.selectList(wrapper);
        ValidUtil.assertListIsNull(list, "租户已经入驻！直接登录");

        //1.3 管理员已经存在  方案1（采纳）：让他直接登录进去添加机构 方案2：进行密码比对，直接关联即可。
        Employee employee = enteringDto.getEmployee();

        List<Employee> es = employeeMapper.selectList(new EntityWrapper<Employee>()
                .eq("username", employee.getUsername())
                .or()
                .eq("email", employee.getEmail())
                .or()
                .eq("tel", employee.getTel())
        );
        ValidUtil.assertListIsNull(es, "您已经入驻过店铺了！登录后可以添加新的店铺！");
        //2 调用授权服务里面登录用户服务添加用户，并返回登录id
        LoginUser loginUser = new LoginUser();
        loginUser.setUsername(employee.getUsername());
//        loginUser.setPassword(employee.getPassword());
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String enPass = bCryptPasswordEncoder.encode(employee.getPassword());
        loginUser.setPassword(enPass);
        loginUser.setType(0);
        AjaxResult ajaxResult = loginUserClient.addOrUpdate(loginUser);
        if (!ajaxResult.isSuccess()) {
            throw  new RuntimeException(ajaxResult.getMessage());
        }

        Long loginUserId = Long.valueOf(ajaxResult.getResultObj().toString());
        //3 添加管理员
        employee.setLoginId(loginUserId);
        employee.setPassword(enPass);
        employeeMapper.insert(employee);
        //4 添加租户
        tenant.setAdminId(employee.getId());
        tenantMapper.insert(tenant);
        //5 租户与套餐直接中间表
        Map<String,Long> params = new HashMap<>();
        params.put("tenantId",tenant.getId());
        params.put("mealId",enteringDto.getMealId());
        tenantMapper.saveTenatMeals(params);
    }
}
