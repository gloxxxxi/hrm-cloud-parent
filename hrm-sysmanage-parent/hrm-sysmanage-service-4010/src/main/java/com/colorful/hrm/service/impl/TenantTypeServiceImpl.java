package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.TenantType;
import com.colorful.hrm.mapper.TenantTypeMapper;
import com.colorful.hrm.service.ITenantTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 租户(机构)类型表 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
@Service
public class TenantTypeServiceImpl extends ServiceImpl<TenantTypeMapper, TenantType> implements ITenantTypeService {

}
