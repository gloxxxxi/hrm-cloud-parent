package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.Meal;
import com.colorful.hrm.mapper.MealMapper;
import com.colorful.hrm.service.IMealService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
@Service
public class MealServiceImpl extends ServiceImpl<MealMapper, Meal> implements IMealService {

}
