package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.OperLog;
import com.colorful.hrm.mapper.OperLogMapper;
import com.colorful.hrm.service.IOperLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志记录 服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
@Service
public class OperLogServiceImpl extends ServiceImpl<OperLogMapper, OperLog> implements IOperLogService {

}
