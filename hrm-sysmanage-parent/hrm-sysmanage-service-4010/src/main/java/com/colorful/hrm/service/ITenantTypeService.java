package com.colorful.hrm.service;

import com.colorful.hrm.entity.TenantType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 租户(机构)类型表 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface ITenantTypeService extends IService<TenantType> {

}
