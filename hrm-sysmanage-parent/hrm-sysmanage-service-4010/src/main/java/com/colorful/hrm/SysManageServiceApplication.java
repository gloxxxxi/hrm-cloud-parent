package com.colorful.hrm;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author SuperJellyfish
 * @description 启动类 - 系统中心
 * @date 2022/1/2 11:05
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.colorful.hrm.mapper")
public class SysManageServiceApplication {
    static Logger logger= LoggerFactory.getLogger(SysManageServiceApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SysManageServiceApplication.class, args);
    }
}
