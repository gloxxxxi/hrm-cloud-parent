package com.colorful.hrm.controller;

import com.colorful.hrm.dto.TenantEnteringDto;
import com.colorful.hrm.service.ITenantService;
import com.colorful.hrm.entity.Tenant;
import com.colorful.hrm.query.TenantQuery;
import com.colorful.hrm.util.AjaxResult;
import com.colorful.hrm.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tenant")
public class TenantController {
    @Autowired
    public ITenantService tenantService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    /**
     * 保存和修改公用的
     * @param tenant  传递的实体
     * @return Ajaxresult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Tenant tenant){
        try {
            if( tenant.getId()!=null) {
                tenantService.updateById(tenant);
            } else {
                tenantService.insert(tenant);
            }
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setMessage("保存对象失败！"+e.getMessage());
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            tenantService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setMessage("删除对象失败！"+e.getMessage());
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public Tenant get(@PathVariable("id")Long id)
    {
        return tenantService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping
    public List<Tenant> list(){

        return tenantService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<Tenant> json(@RequestBody TenantQuery query)
    {
        return tenantService.selectPageList(query);
    }

    /**
     * 入驻
     *      // @Valid 校验注解
     * @param enteringDto
     * @return
     */
    @PostMapping("/entering")
    public AjaxResult entering(@RequestBody @Valid TenantEnteringDto enteringDto){
        tenantService.entering(enteringDto);
        return AjaxResult.me();
    }
}
