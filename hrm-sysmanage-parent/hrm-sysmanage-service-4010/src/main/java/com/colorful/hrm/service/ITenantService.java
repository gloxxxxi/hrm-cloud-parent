package com.colorful.hrm.service;

import com.colorful.hrm.dto.TenantEnteringDto;
import com.colorful.hrm.entity.Tenant;
import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.query.TenantQuery;
import com.colorful.hrm.util.PageList;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface ITenantService extends IService<Tenant> {
    PageList<Tenant> selectPageList(TenantQuery query);

    void entering(TenantEnteringDto enteringDto);
}
