package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.TenantType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 租户(机构)类型表 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface TenantTypeMapper extends BaseMapper<TenantType> {

}
