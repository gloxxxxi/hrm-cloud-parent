package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.Department;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface DepartmentMapper extends BaseMapper<Department> {

}
