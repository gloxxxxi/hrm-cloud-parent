package com.colorful.hrm.service;

import com.colorful.hrm.entity.Employee;
import com.baomidou.mybatisplus.service.IService;
import com.colorful.hrm.vo.UserContextInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface IEmployeeService extends IService<Employee> {

    UserContextInfo getEmpAndTenantByLoginId(Long loginId);
}
