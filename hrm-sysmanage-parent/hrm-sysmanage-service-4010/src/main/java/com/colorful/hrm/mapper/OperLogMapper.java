package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.OperLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 操作日志记录 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface OperLogMapper extends BaseMapper<OperLog> {

}
