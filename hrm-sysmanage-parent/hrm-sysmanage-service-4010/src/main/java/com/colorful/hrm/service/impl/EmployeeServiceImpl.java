package com.colorful.hrm.service.impl;

import com.colorful.hrm.entity.Employee;
import com.colorful.hrm.entity.Tenant;
import com.colorful.hrm.mapper.EmployeeMapper;
import com.colorful.hrm.service.IEmployeeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.colorful.hrm.vo.UserContextInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

    @Resource
    private EmployeeMapper employeeMapper;
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean insert(Employee employee) {
        return super.insert(employee);
    }
    @Override
    public UserContextInfo getEmpAndTenantByLoginId(Long loginId) {
        //查看员工及租户信息
        Employee  employee = employeeMapper
                .loadEmpAndTenantByLoginId(loginId);
        //封装为要返回UserContextInfo
        UserContextInfo result = new UserContextInfo();
        result.setEmployeeId(employee.getId());
        result.setEmpUsername(employee.getUsername());
        Tenant tenant = employee.getTenant();
        if (tenant != null) {
            result.setTenantId(tenant.getId());
            result.setTenantName(tenant.getCompanyName());
        }
        return result;
    }
}
