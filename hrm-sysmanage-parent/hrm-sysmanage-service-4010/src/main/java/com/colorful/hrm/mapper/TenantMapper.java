package com.colorful.hrm.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.colorful.hrm.entity.Tenant;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.colorful.hrm.query.TenantQuery;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface TenantMapper extends BaseMapper<Tenant> {
    List<Tenant> loadPageList(Page<Tenant> page, TenantQuery query);

    void saveTenatMeals(Map<String, Long> params);
}
