package com.colorful.hrm.mapper;

import com.colorful.hrm.entity.Config;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface ConfigMapper extends BaseMapper<Config> {

}
