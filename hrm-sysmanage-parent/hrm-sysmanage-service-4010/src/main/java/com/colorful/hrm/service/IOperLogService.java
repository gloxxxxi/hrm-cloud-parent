package com.colorful.hrm.service;

import com.colorful.hrm.entity.OperLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 操作日志记录 服务类
 * </p>
 *
 * @author super-jellyfish
 * @since 2022-01-02
 */
public interface IOperLogService extends IService<OperLog> {

}
