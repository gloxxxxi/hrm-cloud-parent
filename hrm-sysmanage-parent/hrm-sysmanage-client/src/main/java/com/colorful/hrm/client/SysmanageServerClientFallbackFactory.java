package com.colorful.hrm.client;

import com.colorful.hrm.vo.UserContextInfo;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class SysmanageServerClientFallbackFactory implements FallbackFactory<SysmanageServerClient> {
    @Override
    public SysmanageServerClient create(Throwable throwable) {
        return new SysmanageServerClient() {
            @Override
            public UserContextInfo getEmpAndTenantByLoginId(Long loginId) {
              return null;
            }
        };
    }
}