package com.colorful.hrm.client;

import com.colorful.hrm.vo.UserContextInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "SYSMANAGE-SERVICE",fallbackFactory = SysmanageServerClientFallbackFactory.class)
public interface SysmanageServerClient {
    @GetMapping("/employee/empAndTenant/{loginId}")
    UserContextInfo getEmpAndTenantByLoginId(@PathVariable("loginId")Long loginId);
}