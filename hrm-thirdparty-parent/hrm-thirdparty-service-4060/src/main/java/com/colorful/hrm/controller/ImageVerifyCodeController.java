package com.colorful.hrm.controller;

import com.colorful.hrm.service.IImageVerifyCodeService;
import com.colorful.hrm.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/imageVerifyCode")
public class ImageVerifyCodeController {

    @Autowired
    private IImageVerifyCodeService imageVerifyCodeService;

    @GetMapping("/{imageCodeKey}")
    public AjaxResult getCodeByKey(@PathVariable("imageCodeKey")String imageCodeKey){
        return imageVerifyCodeService.getCodeByKey(imageCodeKey);
    }
    @PostMapping("/verify") //校验图片验证码
    public AjaxResult verifyCode(@RequestBody Map<String,String> params){
        return imageVerifyCodeService.verifyCode(params);
    }
}
