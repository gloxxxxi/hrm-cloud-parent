package com.colorful.hrm.service;


import com.colorful.hrm.util.AjaxResult;

import java.util.Map;

public interface ISmsVerifyCodeService {
    AjaxResult verifySmsCode(Map<String,String> params);

    AjaxResult sendCode(Map<String, String> params);
}
