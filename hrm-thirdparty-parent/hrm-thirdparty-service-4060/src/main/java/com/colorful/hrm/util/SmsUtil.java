package com.colorful.hrm.util;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

/**
 * 短信验证码发送工具
 */
public class SmsUtil {

    //用户名
    private static final String UID = "tom123456";
    //密钥
    private static final String KEY = "d41d8cd98f00b204e980";

    public static void sendMsg(String phone,String smsText) {

        PostMethod post = null;
        try {
            HttpClient client = new HttpClient();
            post = new PostMethod("http://utf8.api.smschinese.cn");
            post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf8");//在头文件中设置转码
            NameValuePair[] data ={ new NameValuePair("Uid", UID),new NameValuePair("Key", KEY),
                    new NameValuePair("smsMob",phone),new NameValuePair("smsText",smsText)};
            post.setRequestBody(data);

            client.executeMethod(post);

            String result = new String(post.getResponseBodyAsString().getBytes("utf8"));

            System.out.println(result); //打印返回消息状态

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (post != null) {
                post.releaseConnection();
            }
        }
    }
}
