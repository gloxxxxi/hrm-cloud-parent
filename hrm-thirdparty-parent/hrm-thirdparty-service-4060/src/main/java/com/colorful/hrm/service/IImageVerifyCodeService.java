package com.colorful.hrm.service;


import com.colorful.hrm.util.AjaxResult;

import java.util.Map;

public interface IImageVerifyCodeService {
    AjaxResult getCodeByKey(String imageCodeKey);

    AjaxResult verifyCode(Map<String, String> params);
}
