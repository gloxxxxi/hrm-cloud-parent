package com.colorful.hrm.handler;

import com.colorful.hrm.config.RabbitMQConfig;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageHandler {

    //短信处理
//    @RabbitListener(queues = {RabbitMQConfig.QUEUE_NAME_SMS},containerFactory = "rabbitListenerContainerFactory")
    @RabbitListener(queues = {RabbitMQConfig.QUEUE_NAME_SMS})
    public void smsHandller(List<Long> ids, Message msg, Channel channel){
        //以后对接推荐系统 根据课程找到要推送用户完成消息推送，现在模拟一些就ok
        //1.准备要发送的消息
        String message = "亲爱的用户，我们课程【"+ids+"】课程已经上线，请查收。www.itsource.cn【colorful】";
        //2.去用户(门户网站用户)表查询用户的邮箱：待定
        //3.发送邮件
        System.out.println("邮件发送："+message);
    }
    //email处理
//    @RabbitListener(queues = {RabbitMQConfig.QUEUE_NAME_EMAIL},containerFactory = "rabbitListenerContainerFactory")
    @RabbitListener(queues = {RabbitMQConfig.QUEUE_NAME_EMAIL})
    public void emailHandller(List<Long> ids, Message msg, Channel channel){
        //1.准备要发送的消息
        String message = "亲爱的用户，我们课程【"+ids+"】课程已经上线，请查收。www.colorful.cn【colorful】";
        //2.去用户(门户网站用户)表查询用户的手机号：待定
        //3.发送邮件
        System.out.println("短信发送："+message);
    }
    //站内信处理
//    @RabbitListener(queues = {RabbitMQConfig.QUEUE_NAME_SYSTEM_MESSAGE},containerFactory = "rabbitListenerContainerFactory")
    @RabbitListener(queues = {RabbitMQConfig.QUEUE_NAME_SYSTEM_MESSAGE})
    public void systemHandller(List<Long> ids, Message msg, Channel channel){
        //1.准备要发送的消息
        String message = "亲爱的用户，我们课程【"+ids+"】课程已经上线，请查收。www.colorful.cn【colorful】";
        //2.获取到用户的ID：
        //消息表：  id, 内容 ，用户ID ，已读/未读 ， 链接
        //          1           1       0         www.xxx.xxx.com
        //3.往用户的站内信表保存内容
        System.out.println("站内信发送："+message);
    }
}
