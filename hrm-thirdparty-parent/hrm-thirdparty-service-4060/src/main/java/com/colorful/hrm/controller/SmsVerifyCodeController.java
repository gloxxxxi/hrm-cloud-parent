package com.colorful.hrm.controller;

import com.colorful.hrm.service.ISmsVerifyCodeService;
import com.colorful.hrm.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/smsVerifyCode")
public class SmsVerifyCodeController {

    @Autowired
    private ISmsVerifyCodeService smsVerifyCodeService;

    @PostMapping("/verify")
    public AjaxResult verifySmsCode (@RequestBody Map<String,String> params){
        return smsVerifyCodeService.verifySmsCode(params);
    }
    @PostMapping("/sendVerifyCode") //发送短信验证码
    public AjaxResult sendVerifyCode(@RequestBody Map<String,String> params){
        return smsVerifyCodeService.sendCode(params);
    }
}
