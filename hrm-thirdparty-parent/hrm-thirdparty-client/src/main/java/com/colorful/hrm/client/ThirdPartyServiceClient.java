package com.colorful.hrm.client;

import com.colorful.hrm.util.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(value = "THIRDPARTY-SERVICE",fallbackFactory = ThirdPartyServiceClientFallbackFactory.class )
public interface ThirdPartyServiceClient {
    //校验图片验证码
    @PostMapping("/imageVerifyCode/verify")
    AjaxResult verifyImgCode(@RequestBody Map<String,String> params);
    @PostMapping("/smsVerifyCode/verify")
    AjaxResult verifySmsCode(@RequestBody Map<String,String> params);
    @PostMapping("/smsVerifyCode/sendVerifyCode") //发送短信验证码
    AjaxResult sendSmsVerifyCode(@RequestBody Map<String,String> params);
}
