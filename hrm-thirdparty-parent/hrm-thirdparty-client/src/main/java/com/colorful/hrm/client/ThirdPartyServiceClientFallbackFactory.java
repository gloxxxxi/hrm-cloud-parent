package com.colorful.hrm.client;

import com.colorful.hrm.util.AjaxResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class ThirdPartyServiceClientFallbackFactory implements FallbackFactory<ThirdPartyServiceClient> {
    @Override
    public ThirdPartyServiceClient create(Throwable throwable) {
        return  new ThirdPartyServiceClient() {
            @Override
            public AjaxResult verifyImgCode(Map<String, String> params) {
                return AjaxResult.me().setSuccess(false).setMessage("系统异常！"+throwable.getMessage());
            }

            @Override
            public AjaxResult verifySmsCode( Map<String,String> params) {
                return AjaxResult.me().setSuccess(false).setMessage("系统异常！"+throwable.getMessage());
            }

            @Override
            public AjaxResult sendSmsVerifyCode(Map<String, String> params) {
                return AjaxResult.me().setSuccess(false).setMessage("系统异常！"+throwable.getMessage());
            }
        };
    }
}
